<style>
    a {
        color:#333;
    }
</style>
<link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
<div style="width:100%;background-color:#eaeaea;padding-top:5%;padding-bottom:5%;">
    <div style="margin-left:5%;margin-right:5%;width:90%;background-color:#fff;">
        <div style="width:100%;border-bottom:1px solid #eaeaea;text-align:center;">
            <img src="%baseUrl%/images/banner.svg" style="width:100px;padding:10px;" />
        </div>
        <div style="padding:20px;text-align:center;font-family:Raleway;color:#000;">
            Hi <b>%name%</b><br/>
            <br/>
            Click below to reset your password.<br/>
            <br/>
            <a href="%baseAUrl%/reset/%code%">Click here</a><br/>
            <br/>
            <p>If the above link does not work, copy and paste the following link in your browser:<br/>
            %baseAUrl%/reset/%code%<br/>
            <br/>
            Your Mark Labs Team<br/>
            www.marklabs.co
        </div>
        <div style="padding:20px;border-top:1px solid #eaeaea;text-align:center;font-family:Raleway;color:#999;font-size:12px;">
            &copy; 2016 Mark Labs Inc.<br/>
        </div>
    </div>
</div>
