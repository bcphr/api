var updDb = require("../db/updates");
var sessionSv = require("./sessions");

exports.saveUpdate = function(req, command, object, callback) {
	sessionSv.getUser(req, function(err, user) {
		var save = {
			userId: user.uuid,
			oupdated: Date.now(),
			command: command,
			object: object
		};
		updDb.saveDocument(save, function(err, message) {
			if (callback) callback(err, message);
		});
	});
}
