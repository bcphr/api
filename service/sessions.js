var userDb = require("../db/users")
var userService = require("./users")
var jwt = require("jsonwebtoken");
var config = require("../config");

exports.getToken = function(req) {
	return req.headers["x-access-token"] || req.query.token;
}

exports.getUser = function(req, callback) {
	jwt.verify(exports.getToken(req), config.server.secret, function(err, decoded) {
		if (!err) {
			userDb.findDocumentById(decoded.userId, function(err, user) {
				callback(err, userService.getUser(user), user);
			})
		}
		else {
			callback(err, decoded);
		}
	});
};

exports.getFullUser = function(req, callback) {
	jwt.verify(exports.getToken(req), config.server.secret, function(err, decoded) {
		if (!err) {
			userDb.findDocumentById(decoded.userId, function(err, user) {
				callback(err, user);
			})
		}
		else {
			callback(err, decoded);
		}
	});
};

exports.createSession = function(user, callback) {
	var token = jwt.sign({userId: user.uuid}, config.server.secret,
		{
			expiresIn: user.tokenExpiry || config.server.tokenExpiry // expires in 24 hours
        }
    );
	callback(null, token);
};

exports.delete = function(req, callback) {
	// sessionsDb.removeDocument({uuid: token}, function() {
	// 	callback();
	// });
	callback();
};