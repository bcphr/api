var aws = require('aws-sdk');
var config = require('../config');

console.log("Loading AWS with ",config.awsConfig);

// load aws config
aws.config = new aws.Config(config.awsConfig);

module.exports = aws;
