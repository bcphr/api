//aws instance
var awssdk = require("./awssdk");
//general zzish config
var config = require("../config.js");

//uuid generatior
var uuid = require("node-uuid");

var fs = require("fs");


var CONTACT_EMAIL = "admin@" + config.server.domain;

var FROM_EMAIL = "'Mark Team' <" + CONTACT_EMAIL + ">";

exports.sendActualEmail = function(toArray, subject, html, text, callback) {
	//"var emailCheck = process.env.sendEmail;
	var sendEmail = function(to){
		var fromUrl = config.server.webUrl;

		html = parseData(html, {"baseUrl": fromUrl});
		html = parseData(html, {"baseAUrl": fromUrl + "/#!/"});
		html = parseData(html, {"contactemail": CONTACT_EMAIL});


		text = parseData(text, {"baseUrl": fromUrl});
		text = parseData(text, {"baseAUrl": fromUrl + "/#!"});
		text = parseData(text, {"contactemail": CONTACT_EMAIL});

		console.log("AAAA", text);

		if (config.awsConfig.aws_enabled === "true") {
			// load AWS SES
			var ses = new awssdk.SES();

			var dest = { ToAddresses: [to] };
            dest.BccAddresses = ["emails@" + config.server.domain];

			var new_id = uuid.v4() + ".html";
			var url = config.server.apiUrl + "/emails/" + new_id;
			var fileToWrite = "./public/emails/" + new_id;

			console.log("WRITING TO " + fileToWrite);


			var emaillink =	"<p style=\"margin-top:40px; text-align:center;\"><a href=\"%emaillink%\" style=\"color:#999;font-size:12px;\">Can't read this? Click here to view this in a browser</a></p>";

			fs.writeFile(fileToWrite, html, function(err, result) {
				console.log("ERR", err, result);
			});

			html = emaillink + html;

			html = parseData(html, {"emaillink": url});

			var message = {
				Source: FROM_EMAIL,
				Destination: dest,
				Message: {
					Subject: {
						Data: subject
					},
					Body: {
						Text: {
							Data: text
						},
						Html: {
							Data: html
						}
					}
				}
			};

			//console.log("Sending", message);
			console.log("Sending", message);

			// this sends the email
			ses.sendEmail(
				message
			, function(err, data) {
				if(err) {
					console.log("Error sending email: " + err);
					if (callback) callback("ERROR", err);
				} else {
					console.log("Email sent:");
					console.log(data);
					if (callback) callback(null, "Email sent:");
				}
			});
		}
		else {
			console.log("Email not enabled");
		}
	};
	toArray.forEach(sendEmail);
};


function parseData(input, params) {
	for (var i in params) {
		var patt = new RegExp("%" + i + "%", "g" );
		if (input === undefined) {
			console.log("BAD INPUT", input);
		}
		else {
			input = input.replace(patt, params[i]);
		}
	}
	return input;
}

exports.sendEmailTemplate = function(email, subject, doc, params, htmlParams, callback) {
	var emailFolder = config.server.baseDir + "/emails";
	fs.stat(emailFolder + "/html/" + doc + ".txt", function(err) {
		if (!err) {
			fs.readFile(emailFolder + "/html/" + doc + ".txt", "utf-8", function(err1, contents) {
				var htmlText = contents;
				fs.readFile(emailFolder + "/text/" + doc + ".txt", "utf-8", function(err2, contents2) {
					var txtText = contents2;
					if (!htmlParams) htmlParams = params;
					exports.sendActualEmail(email, parseData(subject, params), parseData(htmlText, htmlParams), parseData(txtText, params), callback);
				});
			});
		} else {
			fs.readFile(emailFolder + "/text/" + doc + ".txt", "utf-8", function(err1, contents) {
				exports.sendActualEmail(email, subject, parseData(contents, params), parseData(contents, params), callback);
			});
		}
	});
};
