var userDb = require("../db/users");
var secSv = require("./security");
var sessionSv = require("./sessions");
var UUID = require("node-uuid");

exports.getName = function(user) {
	if (user.firstname) {
		return user.firstname;
	}
	else {
		return user.email;
	}
}

exports.loadUser = function(req, user, auser, requester, token, callback) {
	callback(user);
};

exports.saveUser = function(req, res, input, user) {
	userDb.findDocumentById(input.uuid, function(err, dbuser) {
		input.uuid = dbuser.uuid;
		db.saveRecord(req, res, userDb, null, null, input);
	});
}

exports.createUser = function(email, password) {
	return {email: email, password: secSv.encrypt(password), verified: false, preferences: {}, verifyCode: UUID.v4()};
}

exports.getUser = function(user, start) {
	var userSstart = (start || []).filter(function(x) {return x.userId === user.uuid})[0];
	var result = JSON.parse(JSON.stringify(userSstart || {}));
	var user = {
		firstname: user.firstname,
		surname: user.surname,
		email: user.email,
		uuid: user.uuid,
		preferences: user.preferences,
		created: user.created,
		image: user.image
	};
	Object.assign(result, user);
	return result;
}

exports.searchQueryEmail = function(email) {
	email = email.replace("+","\\+");
    return {"email": { $regex: new RegExp("^" + email, "i") } };
}

exports.validateEmail = function(email) {
    return {"email": { $regex: new RegExp("^" + email, "i") } };
}