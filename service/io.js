var uuid = require("node-uuid");
var config = require("../config");
var fileSystem = require("fs")
var awssdk = require("./awssdk");
var path = require("path");
var fs = require("fs");
var fileDb = require("../db/downloads");
var db = require("../db/dbview");

var sessionSv = require("./sessions");

var saveRecord = function(uuid_key, input, aws_enabled, postfix, callback) {
    var record = {
        uuid: uuid_key,
        name: input.name,
        format: input.type,
        aws: aws_enabled,
        postfix: postfix
    };
    fileDb.saveDocument(record, function(err, message) {
        callback(err, record);
    });
}

exports.upload = function(data, dataformat, postfix, input, callback) {
	var uuid_key = uuid.v4();
	complete_filename = config.server.baseDir + "/tmp/" + uuid_key + postfix;

    fs.writeFile(complete_filename, data, dataformat, function(err) {
        var bodyStream = fs.createReadStream(complete_filename);
        var stat = fileSystem.statSync(complete_filename);
        if (config.awsConfig.aws_enabled === "true") {
            var s3bucket = new awssdk.S3();
            s3bucket.putObject({
                Bucket: config.awsConfig.uploadbucket,
                Key: uuid_key + postfix,
                ContentLength: stat.size,
                ACL:'public-read-write',
                Body: bodyStream
            }, function (err, resp) {
                if (!err) {
                    saveRecord(uuid_key, input, config.awsConfig.aws_enabled, postfix, callback);
                }
                else {
                    callback(err, resp);
                }
            });
        }
        else {
            fs.writeFile(config.server.baseDir + "/public/uploads/" + uuid_key + postfix, data, dataformat, function() {
                saveRecord(uuid_key, input, config.aws, postfix, callback);
            });
            //save to public folder
        }
    });
}

exports.export = function(req, res, filename, format, data) {
	var uuid_key = uuid.v4();
	var complete_filename = config.server.baseDir + "/tmp/" + uuid_key;

	fs.writeFile(complete_filename, data, function(err) {
		var stat = fileSystem.statSync(complete_filename);

	    res.writeHead(200, {
	        "Content-Disposition": "attachment;filename=" + filename,
	        "Content-Type": format,
	        "Content-Length": stat.size
	    });

	    var readStream = fileSystem.createReadStream(complete_filename);
	    // We replaced all the event handlers with a simple call to readStream.pipe()
	    readStream.pipe(res);
	});
}

exports.extract = function(req, res, filename, format, data, callback) {
    var uuid_key = uuid.v4();
    var complete_filename = config.server.baseDir + "/tmp/" + uuid_key;

    fs.writeFile(complete_filename, data, function(err) {
        var record = {
            uuid: uuid_key,
            name: filename,
            format: format,
        };
        console.log("Saving", record);
        fileDb.saveDocument(record, function(err, message) {
            db.processDb(err, message, res, record);
        });
    });
}

exports.download = function(req, res, uuid) {
    fileDb.findDocumentById(uuid, function(err, resp) {
        if (!err && resp) {
            var complete_filename = config.server.baseDir + "/tmp/" + uuid + resp.postfix;
            try {
                var stat = fileSystem.statSync(complete_filename);

                res.writeHead(200, {
                    "Content-Disposition": "attachment;filename=" + resp.name,
                    "Content-Type": resp.format,
                    "Content-Length": stat.size
                });
                var readStream = fileSystem.createReadStream(complete_filename);
                // We replaced all the event handlers with a simple call to readStream.pipe()
                fileDb.removeDocument({uuid: uuid}, function(err, resp) {
                    readStream.pipe(res, function(err) {
                        fs.unlink(complete_filename);
                    });
                });
            }
            catch (e) {
                console.log(e);
                console.log("No file");
                res.status(400).send({code: "nofile"});
            }
        }
        else {
            res.status(400).send(resp);
        }
    })
}

exports.view = function(req, res, uuid) {
    fileDb.findDocumentById(uuid, function(err, resp) {
        if (!err && resp) {
            var complete_filename = config.server.baseDir + "/tmp/" + uuid + resp.postfix;
            try {
                var stat = fileSystem.statSync(complete_filename);
                res.writeHead(200, {
                    "Content-Disposition": "attachment;filename=" + resp.name,
                    "Content-Type": resp.format,
                    "Content-Length": stat.size
                });

                var readStream = fileSystem.createReadStream(complete_filename);
                // We replaced all the event handlers with a simple call to readStream.pipe()
                readStream.pipe(res, function(err) {

                });
            }
            catch (e) {
                console.log(e);
                console.log("No file");
                res.status(400).send({code: "nofile"});
            }
        }
        else {
            res.status(400).send(resp);
        }
    })
}