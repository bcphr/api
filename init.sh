export APP_TEMPLATE_AWSKEY="1";
export APP_TEMPLATE_AWSSECRET="2";
export APP_TEMPLATE_REGION="3";
export APP_TEMPLATE_AWS_ENABLED="4";
export APP_TEMPLATE_UPLOAD_BUCKET="5";
export APP_TEMPLATE_BASE_DIR="6";
export APP_TEMPLATE_WEB_URL="7";
export APP_TEMPLATE_API_URL="8";
export APP_TEMPLATE_PORT="9";
export APP_TEMPLATE_DB_NAME="10";
export APP_TEMPLATE_DB_PORT="11";
export APP_TEMPLATE_DB_HOST="12";