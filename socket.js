// var logger = require("./logger");
// var teamDb = require("./db/teams");
// var eventDb = require("./db/events");
// var meetingDb = require("./db/meetings");
// var async = require("async");
// var THROTTLE = 1000;
// var TIMEOUT = 750;
// var MAX_QUEUE_LENGTH = 10;

// var clients = [];

// var loadSocketForUser = function(socket, data) {
// 	var socks = {
// 		socket: socket,
// 		userId: data.userId,
// 		lastSent: null,
// 		queue: []
// 	};
// 	clients.push(socks);
// };

// var getSocketsForUser = function(uuid) {
// 	return clients.filter(function(client) {
// 		return client.userId === uuid;
// 	});
// };

// exports.onConnect = function(socket) {
// 	socket.emit("news", { hello: "world" });

// 	// socket.on("poll", function (data) {
// 	// 	clients.forEach(function(client) {
// 	// 		if (client.socket.id === data.id) {
// 	// 			client.socket.emit("poll", { id: socket.id });
// 	// 			client.lastemit = Date.now();
// 	// 		}
// 	// 	});
// 	// });

// 	//Client connects
// 	socket.on("kconnect", function (data) {
// 		logger.info("**BCP on socket", data);
// 		loadSocketForUser(socket, data);
// 		socket.emit("pinit", { id: socket.id });
// 	});

// 	//when disconnect remove from clients
// 	socket.on("disconnect", function() {
// 		var index;

// 		for(var i = 0; i < clients.length; i++) {
// 			if (clients[i].socket.id === socket.id) {
// 				index = i;
// 				break;
// 			}
// 		}

//         //Previously this was crashing (trying to call with without a client?)
//         if(clients !== undefined) {
//             if (clients[index] !== undefined) {
// 				logger.info("Client gone (id=" + socket.id + "). userId: " + clients[index].userId);
// 				clients.splice(index, 1);
//             }
//         }
// 	});

// 	socket.on("kdisconnect", function(data) {
// 		var index = -1;
// 		var i = 0;
// 		clients.forEach(function(client) {
// 			if (client.socket.id === data.id) {
// 				index = i;
// 			}
// 			i++;
// 		});
// 		if (index != -1) {
// 			clients.splice(index, 1);
// 		}
// 	});

// 	socket.on("error", function(exception) {
// 		logger.err("Exception from socket.io", exception, this);
// 	});
// };

// var sendSocket = function(client, data, callback) {
// 	if (client.lastSent === null || client.lastSent + THROTTLE < Date.now() || client.queue.length > MAX_QUEUE_LENGTH) {
// 		client.lastSent = Date.now();
// 		var dataq = [];
// 		if (client.queue.length > 0) {
// 			dataq = client.queue.splice(0,client.queue.length);
// 		}
// 		dataq.push(data);
// 		var q = {
// 			"data": dataq
// 		};
// 		client.socket.emit("process", q);
// 	}
// 	else{
// 		client.queue.push(data);
// 		setTimeout(function() {
// 			clients.forEach(function(client) {
// 				if (client.queue.length > 0) {
// 					var q = {
// 						"data": client.queue.splice(0,client.queue.length)
// 					};
// 					// client.socket.emit("process", q, function(data) {
// 					// 	console.log("DONE");
// 					// });
// 					client.socket.emit("process", q);
// 					client.counter++;
// 				}
// 			});
// 		}, TIMEOUT);
// 	}
// 	callback();
// };

// exports.broacastToTeamObject = function(team, command, model, object, callback) {
// 	var toEmit = {
// 		command: command,
// 		model: model,
// 		data: object
// 	};
// 	if (team.members) {
// 		async.eachSeries(team.members, function(member, icallback) {
// 			if (!member.userId) {
// 				icallback();
// 			}
// 			else {
// 				async.eachSeries(getSocketsForUser(member.userId), function(client, icallback2) {
// 					sendSocket(client, toEmit, icallback2);
// 				}, function() {
// 					icallback();
// 				});
// 			}
// 		}, function() {
// 			if (callback) callback();
// 		});
// 	}
// 	else {
// 		if (callback) callback();
// 	}
// };

// //this is for when the user object has been updated
// exports.broadcastUser = function(object, callback) {
// 	//get all the users that this user is a team of and broadcast to all the teams
// 	teamDb.searchMany({"members.userId": object.uuid}, function(err, teams) {
// 		async.eachSeries(teams, function(team, icallback) {
// 			exports.broacastToTeamObject(team, "update", "member", object, icallback);
// 		}, function() {
// 			exports.broadcastToUser(object.uuid, "update", "user", object, function() {
// 				if (callback) callback();
// 			});
// 		});
// 	});
// };


// //when a team has been updated
// //when an event has been updated
// exports.broadcastToUsers = function(users, command, model, object, callback) {
// 	//get all the users that this user is a team of and broadcast to all the teams
// 	var toEmit = {
// 		command: command,
// 		model: model,
// 		data: object
// 	};
// 	async.eachSeries(users, function(userId, icallback) {
// 		async.eachSeries(getSocketsForUser(userId), function(client, icallback2) {
// 			sendSocket(client, toEmit, icallback2);
// 		}, function() {
// 			icallback();
// 		});
// 	}, function() {
// 		if (callback) callback();
// 	});
// };


// //commiemtns, actions
// exports.broadcastToUser = function(userId, command, model, object, callback) {
// 	//get all the users that this user is a team of and broadcast to all the teams
// 	var toEmit = {
// 		command: command,
// 		model: model,
// 		data: object
// 	};
// 	async.eachSeries(getSocketsForUser(userId), function(client, icallback) {
// 		sendSocket(client, toEmit, icallback);
// 	}, function() {
// 		if (callback) callback();
// 	});
// };

// //when a member has been added (api/teams)
// exports.broadcastToTeam = function(teamId, command, model, object, callback) {
// 	teamDb.get(teamId, function(err, team) {
// 		exports.broacastToTeamObject(team, command, model, object, callback);
// 	});
// };


// //actions that belong to evnts
// //commiments (for active 1-on-1)
// //notes
// exports.broadcastToEvent = function(eventId, command, model, object, callback) {
// 	var toEmit = {
// 		command: command,
// 		model: model,
// 		data: object
// 	};
// 	eventDb.get(eventId, function(err, event) {
// 		async.eachSeries(event.members, function(userId, icallback) {
// 			async.eachSeries(getSocketsForUser(userId), function(client, icallback2) {
// 				sendSocket(client, toEmit, icallback2);
// 			}, function() {
// 				icallback();
// 			});
// 		}, function() {
// 			if (callback) callback();
// 		});
// 	});
// };

// //goals
// //meeting
// exports.broadcastToMeeting = function(meetingId, command, model, object, callback) {
// 	meetingDb.get(meetingId, function(err, meeting) {
// 		async.eachSeries(meeting.members, function(memberId, icallback) {
// 			async.eachSeries(getSocketsForUser(memberId), function(client, icallback2) {
// 				var toEmit = {
// 					command: command,
// 					model: model,
// 					data: object
// 				};
// 				sendSocket(client, toEmit, icallback2);
// 			}, function() {
// 				icallback();
// 			});
// 		}, function() {
// 			if (callback) callback();
// 		});
// 	});
// };

// //goals/actions/roadblocks
// exports.broadcastMultiple = function(otype, command, model, object, callback) {
// 	if (object.eventId) {
//         exports.broadcastToEvent(object.eventId, command, model, object, function() {
//             callback();
//         });
// 	}
// 	else if (otype === "team" || otype === "pulse") {
// 		//team meeting
//         exports.broadcastToMeeting(object.ownerId, command, model, object, function() {
//             callback();
//         });
// 	}
// 	else {
// 		//1:1
// 		//send to manager and employee
//         exports.broadcastToUsers(object.users, command, model, object, function() {
//             callback();
//         });
// 	}
// };
