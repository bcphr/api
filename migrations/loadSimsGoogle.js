var xlsx = require("xlsx");
var request = require("request");
var workbook = xlsx.readFile('./sim.xlsx');
var async = require("async");
var download = require("download");
var UUID = require("node-uuid");
var fs = require("fs");
var mime = require("mime-types");
var google = require("../google.js");
var dataMap = {};

//db lookups
var userMap = {};
var fundMap = {};
var orgMap = {};
var proMap = {};
var actMap = {};
var tempMap = {};
var fieldMap = {};
var periodMap = {};
var countryMap = {};

//lookups
var outMap = {};
var finMap = {};

var fundDb = require("../db/funders");
var userDb = require("../db/users");
var orgDb = require("../db/organisations");
var secSv = require("../service/security");
var outDb = require("../db/outcomes");
var proDb = require("../db/programmes");
var actDb = require("../db/activities");
var kpiDb = require("../db/kpis");
var temDb = require("../db/templates");
var recDb = require("../db/records");
var stoDb = require("../db/stories");
var graDb = require("../db/grants");
var finDb = require("../db/financing");
var conDb = require("../db/contentTypes");
var dowDb = require("../db/downloads");



var loadOutcomes = function(callback) {
	async.eachSeries(dataMap["outcomes"], function(name, icallback) {
		if (name != "-") {
			var dbrecord = {
				name: name
			};
			outDb.findDocument({name: name}, function(err, doc) {
				outMap[name] = doc;
				icallback();
			});
		}
		else {
			icallback();
		}
	}, function() {
		callback();
	});
}

var loadFinancingTypes = function(callback) {
	async.eachSeries(dataMap["financingType"], function(name, icallback) {
		if (name != "-") {
			finDb.findDocument({uuid: name.toLowerCase()}, function(err, doc) {
				finMap[name] = doc;
				icallback();
			});
		}
		else {
			icallback();
		}
	}, function() {
		callback();
	});
}

var loadFieldTypes = function() {
	dataMap["fieldtype"].forEach(function(type, index) {
		fieldMap[type] = dataMap["fieldtypeId"][index];
	});
}

var loadPeriods = function() {
	dataMap["periods"].forEach(function(type, index) {
		periodMap[type] = dataMap["periodId"][index];
	});
}

var data = function(callback) {
	google('1ebV-InHY6nEyt6U_c8OObYv6B9_xExPYoZS4IpMmWYw', 0, function(data) {
		var columns = ["periods", "kpitype", "fieldtype", "outcomes", "contentType", "financingType", "fieldtypeId", "periodId"]
		data.forEach(function(row) {
			row.forEach(function(col, index) {
				if (!dataMap[columns[index]]) {
					dataMap[columns[index]] = [];
				}
				dataMap[columns[index]].push(col);
			});
		});
		outMap = {};
		loadOutcomes(function() {
			loadFinancingTypes(function() {
				loadFieldTypes();
				loadPeriods();
				loadCountries(callback);
			});
		});
	});
}

var loadCountries = function(callback) {
	google('1ebV-InHY6nEyt6U_c8OObYv6B9_xExPYoZS4IpMmWYw', 12, function(records) {
		records.splice(0, 1);
		records.forEach(function(record) {
			countryMap[record[1]] = record[0];
		});
		callback();
	});
}

var key = function(input) {
	return input ? value(input).toLowerCase() : "";
}

var value = function(input) {
	return input ? input.trim().replace(/\t/,"") : "";
}


var loadFunders = function(data, callback) {
	fundMap = {};
	data.forEach(function(x) {
		var fund = x[1];
		if (!fundMap[key(fund)]) {
			fundMap[key(fund)] = value(fund);
		}
	});
	callback();
}

var loadOrganisations = function(data, callback) {
	orgMap = {};
	data.forEach(function(x) {
		var org = x[3];
		var outcome = x[5];
		var funder = x[1];
		var financing = x[0];
		if (!orgMap[key(org)]) {
			orgMap[key(org)] = {
				name: value(org),
				mission: value(x[4]),
				outcomes: {},
				funders: {},
				financings: {}
			};
		}
		if (!orgMap[key(org)].funders[key(funder)]) {
			orgMap[key(org)].funders[key(funder)] = {
				name: value(funder)
			};
		}
		if (!orgMap[key(org)].financings[key(financing)]) {
			orgMap[key(org)].financings[key(financing)] = {
				name: value(financing)
			};
		}
		if (!orgMap[key(org)].outcomes[key(outcome)]) {
			orgMap[key(org)].outcomes[key(outcome)] = {
				name: value(outcome),
				kpis: []
			};
		}
		orgMap[key(org)].outcomes[key(outcome)].kpis.push({
			metric: x[6],
			targets: x[7],
			impact: x[8]
		});
	});
	console.log(orgMap);
	callback();
}

google('1V63yk89iD0FFsCi0UHNniXYG4RmJZ2LomVCGD0QQEO4', 1, function(sim_rows) {
	data(function() {
		// loadFunders(sim_rows, function() {
		// 	loadOrganisations(sim_rows, function() {
		// 		console.log("DONE");
		// 	});
		// });
	});
});
