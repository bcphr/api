var locations = require("../db/locations");
var fs = require("fs");
var async = require("async");

var data = fs.readFileSync("./countries.tsv","UTF-8");
var rows = data.split("\n").map(function(row) {
	var columns = row.split("\t");
	return {
		name: columns[1],
		uuid: columns[0]
	};
});

var load = function(callback) {
	locations.removeDocument({}, function() {
		async.eachSeries(rows, function(row, icallback) {
			locations.saveDocument(row, function() {
				icallback();
			});
		}, function() {
			console.log("DONE COUNTRIES");
			if (!callback) process.exit(-1);
			else callback();
		});
	});
}

module.exports = load;