var xlsx = require("xlsx");
var async = require("async");
var download = require("download");
var UUID = require("node-uuid");
var fs = require("fs");
var mime = require("mime-types");
var google = require("../google.js");
var dataMap = {};

//db lookups
var userMap = {};
var fundMap = {};
var orgMap = {};
var proMap = {};
var actMap = {};
var tempMap = {};
var fieldMap = {};
var periodMap = {};
var countryMap = {};
var imageMap = {};
var kpiMap = {};
//lookups
var outMap = {};
var finMap = {};

var fundDb = require("../db/funders");
var userDb = require("../db/users");
var orgDb = require("../db/organisations");
var secSv = require("../service/security");
var outDb = require("../db/outcomes");
var proDb = require("../db/programmes");
var actDb = require("../db/activities");
var kpiDb = require("../db/kpis");
var temDb = require("../db/templates");
var recDb = require("../db/records");
var stoDb = require("../db/stories");
var graDb = require("../db/grants");
var finDb = require("../db/financingTypes");
var conDb = require("../db/contentTypes");
var dowDb = require("../db/downloads");
var mtempDb = require("../db/mtemplates");
var graphDb = require("../db/graphs");
var fieldsDb = require("../db/fieldTypes")

var indexes = [1, 7, 30, 120, 360];

var loadOutcomes = function(callback) {
	outDb.findDocuments({}, function(err, docs) {
		docs.forEach(function(doc) {
			outMap[doc.name] = doc;
		});
		callback();
	});
}

var loadFinancingTypes = function(callback) {
	finDb.findDocuments({}, function(err, docs) {
		docs.forEach(function(doc) {
			finMap[doc.name] = doc;
		});
		callback();
	});
}

var loadFieldTypes = function(callback) {
	fieldsDb.findDocuments({}, function(err, docs) {
		docs.forEach(function(doc) {
			fieldMap[doc.name] = doc.uuid;
		});
		callback();
	});
}

var loadPeriods = function() {
	dataMap["periods"].forEach(function(type, index) {
		periodMap[type] = dataMap["periodId"][index];
	});
}


var loadCountries = function(callback) {
	google(googleSheetId, 12, function(records) {
		records.forEach(function(record) {
			countryMap[record[1]] = record[0];
		});
		callback(records);
	});
}

var loadProfiles = function(callback) {
	dataMap["picons"].forEach(function(type, index) {
		imageMap[type] = dataMap["purls"][index];
	});
}

var data = function(callback) {
	google(googleSheetId, 0, function(data) {
		var columns = ["outcomes", "periods", "kpitype", "fieldtype", "contentType", "financingType", "fieldtypeId", "periodId", "icons", "picons", "purls"];
		data.forEach(function(row) {
			row.forEach(function(col, index) {
				if (!dataMap[columns[index]]) {
					dataMap[columns[index]] = [];
				}
				dataMap[columns[index]].push(col);
			});
		});
		columns.forEach(function(x) {
			dataMap[x].splice(0,1);
		})
		outMap = {};
		loadOutcomes(function() {
			loadFinancingTypes(function() {
				loadFieldTypes(function() {
					loadPeriods();
					loadProfiles();
					loadCountries(callback);
				});
			});
		})
	});
}

var saveUser = function(userId, user, icallback) {
	userDb.saveDocument(user, function(err, resp) {
		userMap[userId] = user;
		icallback();
	});
}

var saveImage = function(path, downloadpath, icallback) {
	var lastIndexOf = path.lastIndexOf(".");
	var lastIndexOfFile = path.lastIndexOf("/");
	var postfix = path.substring(lastIndexOf);
	var fileName = path.substring(lastIndexOfFile+1);
	download(downloadpath).then(data => {
		var uuid = UUID.v4();
		var filepath = "../tmp/" + uuid + postfix;
		fs.open(filepath, 'w', function(err, fd) {
		    if (err) {
		        throw 'error opening file: ' + err;
		    }
		    fs.write(fd, data, 0, data.length, null, function(err) {
		        if (err) throw 'error writing file: ' + err;
		        fs.close(fd, function() {
		            var download = {
		            	uuid: uuid,
		            	name: fileName,
		            	format: mime.lookup(filepath), //TODO
		            	aws: null,
		            	postfix: postfix
		            };
					dowDb.saveDocument(download, function() {
						icallback(download);
					});
		        })
		    });
		});
	});
}

var loadFunders = function(callback) {
	google(googleSheetId, 2, function(funders) {
		funders.splice(0, 1);
		google(googleSheetId, 1, function(users) {
			users.splice(0, 1);
			async.eachSeries(users, function(userF, icallback) {
				var image;
				var user = {
					email : userF[1],
					password : secSv.encrypt("" + userF[2]),
					firstname: userF[3],
					surname: userF[4],
					verified : true,
					"image" : image,
					preferences : {

					}
				};
				if (userF[5]) {
					var file = userF[5];
					saveImage(userF[5], imageMap[userF[5]],function(image) {
						user.image = image;
						saveUser(parseInt(userF[0]), user, icallback);
					});
				}
				else {
					saveUser(parseInt(userF[0]), user, icallback);
				}

			}, function() {
				async.eachSeries(funders, function(funderR, icallback) {
					var funder = {
						users : [],
						admins: [],
						name : funderR[1]
					};
					("" + funderR[2]).split(",").forEach(function(x) {
						funder.admins.push({
							userId: userMap[parseInt(x)].uuid,
							status: "enabled"
						});
					});
					("" + funderR[3]).split(",").forEach(function(x) {
						if (x != "") {
							funder.users.push({
								userId: userMap[parseInt(x)].uuid,
								status: "enabled"
							});
						}
					});
					fundDb.saveDocument(funder, function() {
						fundMap[parseInt(funderR[0])] = funder;
						icallback();
					});
				}, function() {
					callback();
				});
			});
		});
	});
}

var loadOrganisations = function(callback) {
	google(googleSheetId, 3, function(organisations) {
		organisations.splice(0, 1);
		google(googleSheetId, 5, function(maps) {
			maps.splice(0, 1);
			async.eachSeries(organisations, function(orgR, icallback) {
				var org = {
					funders: [],
					users : [],
					authorisations: [],
					admins: [],
					name : orgR[1],
					mission: "",
					uuid: UUID.v4()
				};
				org.secureCode = secSv.encrypt(org.uuid);
				("" + orgR[2]).split(",").forEach(function(x) {
					org.admins.push({
						userId: userMap[parseInt(x)].uuid,
						status: "enabled"
					});
				});
				("" + orgR[3]).split(",").forEach(function(x) {
					if (x != "") {
						org.users.push({
							userId: userMap[parseInt(x)].uuid,
							status: "enabled"
						});
					}
				});
				("" + orgR[4]).split(",").forEach(function(x) {
					if (x!="") {
						org.authorisations.push({
							userId: userMap[parseInt(x)].uuid,
							status: "enabled"
						});
					}
				});
				maps.forEach(function(row) {
					if (row[0] === orgR[0]) {
						org.funders.push({
							funderId: fundMap[row[1]].uuid
						});
					}
				});
				if (orgR[5]) {
					org.mission = orgR[5];
				}
				orgDb.saveDocument(org, function() {
					orgMap[orgR[0]] = org;
					icallback();
				});
			}, function() {
				callback();
			});
		});
	});
}


var loadProgrammes = function(callback) {
	google(googleSheetId, 6, function(programmes) {
		programmes.splice(0, 1);
		async.eachSeries(programmes, function(proR, icallback) {
			var pro = {
				organisationId: orgMap[""+proR[0]].uuid,
				name : proR[2],
				outcomeId: outMap[proR[3]].uuid
			};
			proDb.saveDocument(pro, function() {
				proMap[proR[1]] = pro;
				icallback();
			});
		}, function() {
			callback();
		});
	});
}

var loadFinancing = function(callback) {
	google(googleSheetId, 4, function(records) {
		records.splice(0, 1);
		async.eachSeries(records, function(record, icallback) {
			var dbrecord = {
				organisationId : proMap[record[0]].organisationId,
				programmeId : proMap[record[0]].uuid,
				name : record[1],
				amount : parseFloat(record[2]),
				financingId: finMap[record[3]].uuid
			};
			graDb.saveDocument(dbrecord, function() {
				icallback();
			});
		}, function() {
			callback();
		});
	});
}


var loadActivities = function(callback) {
	google(googleSheetId, 7, function(activities) {
		activities.splice(0, 1);
		async.eachSeries(activities, function(actR, icallback) {
			var activity = {
				organisationId: proMap[actR[0]].organisationId,
				programmeId: proMap[actR[0]].uuid,
				name : actR[2],
				description: actR[3]
			};
			actDb.saveDocument(activity, function() {
				actMap[actR[1]] = activity;
				icallback();
			});
		}, function() {
			callback();
		});
	});
}

var loadTemplates = function(callback) {
	google(googleSheetId, 8, function(records) {
		records.splice(0, 1);
		async.eachSeries(records, function(record, icallback) {
			var dbrecord = {
				model : "template",
				organisationId : actMap[record[0]].organisationId,
				programmeId : actMap[record[0]].programmeId,
				activityId : actMap[record[0]].uuid,
				name : record[2],
				fields: []
			}
			var counter = 3;
			while (true) {
				if (record[counter] === undefined || record[counter] === "") {
					break;
				}
				var tokens = record[counter].split(":");
				var field = {
					"uuid" : UUID.v4(),
					"name" : tokens[0],
					"type" : fieldMap[record[counter+1]],
				}
				if (tokens[1]) {
					field.options = tokens[1].split(",").map(function(x) {
						return {
							name: x,
							uuid: UUID.v4()
						};
					});
				}
				dbrecord.fields.push(field);
				counter +=2;
			}
			temDb.saveDocument(dbrecord, function() {
				tempMap[record[1]] = dbrecord;
				icallback();
			});
		}, function() {
			callback();
		});
	});
}

var loadKPIs = function(callback) {
	google(googleSheetId, 9, function(records) {
		records.splice(0, 1);
		async.eachSeries(records, function(record, icallback) {
			var dbrecord = {
				model : "kpi",
				organisationId : actMap[record[0]].organisationId,
				programmeId : actMap[record[0]].programmeId,
				activityId : actMap[record[0]].uuid,
				name : record[2],
				status : "active",
				projection : record[3],
				period : "" + periodMap[record[4]],
				periodStart : record[5].replace(/"/g,""),
				periodStartTime : getTime(record[5]),
				type : record[7].toLowerCase()
			};
			if (record[6]) {
				var template = tempMap[parseInt(record[6])];
				dbrecord.templateId = template.uuid;
				if (record[8]) {
					var row = template.fields.filter(function(x) {
						return x.name === record[8];
					})[0];
					if (row) {
						dbrecord.rowId = row.uuid;
					}
				}
				if (record[9]) {
					var row = template.fields.filter(function(x) {
						return x.name === record[9];
					})[0];
					if (row) {
						dbrecord.rowId2 = row.uuid;
					}
				}
			}
			kpiDb.saveDocument(dbrecord, function() {
				kpiMap[record[1]] = dbrecord;
				icallback();
			});
		}, function() {
			callback();
		});
	});
}



var loadRecords = function(callback) {
	google(googleSheetId, 10, function(records) {
		records.splice(0, 1);
		async.eachSeries(records, function(record, icallback) {
			var template;
			var template = tempMap[record[0]];
			if (template) {
				var dbrecord = {
					model : "record",
					organisationId : template.organisationId,
					programmeId : template.programmeId,
					activityId : template.activityId,
					templateId: template.uuid,
					locationId: countryMap[record[2]],
					timestamp: getTime(record[1]),
					timestampString: record[1].replace(/"/g,""),
					rows: []
				};
				dbrecord.rows = JSON.parse(JSON.stringify(template.fields));
				var counter = 3
				while (true) {
					if (dbrecord.rows[counter - 3] === undefined) {
						break;
					}
					if (dbrecord.rows[counter - 3].type === "radio" || dbrecord.rows[counter - 3].type === "select") {
						dbrecord.rows[counter - 3].value = [record[counter]].map(function(x) {
							return dbrecord.rows[counter - 3].options.filter(function(y) {
								return y.name === ("" + x).replace("/\"/g","");
							})[0].uuid;
						})[0];

					}
					else if (dbrecord.rows[counter - 3].type === "checkbox") {
						dbrecord.rows[counter - 3].values = record[counter].split(",").map(function(x) {
							return dbrecord.rows[counter - 3].options.filter(function(y) {
								return y.name === ("" + x).replace("/\"/g","");;
							})[0].uuid;
						});
					}
					else if (dbrecord.rows[counter - 3].type === "number") {
						dbrecord.rows[counter - 3].number = parseFloat(record[counter]);
					}
					else {
						dbrecord.rows[counter - 3].text = ("" + record[counter]).replace("/\"/g","");;
					}
					counter++;
				}
				recDb.saveDocument(dbrecord, function() {
					icallback();
				});
			}
			else {
				icallback();
			}
		}, function() {
			callback();
		});
	});
}

var loadStories = function(callback) {
	google(googleSheetId, 11, function(stories) {
		stories.splice(0, 1);
		async.eachSeries(stories, function(story, icallback) {
			var model = {
				organisationId: orgMap[story[0]].uuid,
				description: story[2],
				type: story[3].toLowerCase(),
				name: story[1],
				locationId: countryMap[story[4]]
			};
			if (story[3] === "photo" || story[3] === "study") {
				var path = story[5];
				saveImage(story[5], story[5], function(image) {
					var key = story[3] === "photo" ? "image" : "File"
		            model[key] = image;
		            stoDb.saveDocument(model, function() {
						icallback();
					});
				});
			}
			else if (story[3] === "video") {
				model.videoURL = story[5];
				stoDb.saveDocument(model, function() {
					icallback();
				});
			}
			else if (story[3] === "photolink") {
				model.imageURL = story[5];
				model.type = "photo";
				stoDb.saveDocument(model, function() {
					icallback();
				});
			}
			else {
				stoDb.saveDocument(model, function() {
					icallback();
				});
			}
		}, function() {
			callback();
		});
	});
}

var loadFTemplates = function(callback) {
	google(googleSheetId, 13, function(templates) {
		templates.splice(0, 1);
		async.eachSeries(templates, function(record, icallback) {
			var dbrecord = {
				model : "template",
				funderId : fundMap[record[0]].uuid,
				name : record[3],
				fields: [],
				organisations: {}
			}
			var counter = 4;
			record[2].split(",").forEach(function(x) {
				dbrecord.organisations[orgMap[x].uuid] = true;
			});
			while (true) {
				if (record[counter] === undefined || record[counter] === "") {
					break;
				}
				var tokens = record[counter].split(":");
				var field = {
					"uuid" : UUID.v4(),
					"name" : tokens[0],
					"type" : fieldMap[record[counter+1]],
				}
				if (tokens[1]) {
					field.options = tokens[1].split(",").map(function(x) {
						return {
							name: x,
							uuid: UUID.v4()
						};
					});
				}
				dbrecord.fields.push(field);
				counter +=2;
			}
			temDb.saveDocument(dbrecord, function() {
				tempMap[record[1]] = dbrecord;
				icallback();
			});
		}, callback);
	});
}

var getTime = function(time) {
	var x = time.replace(/"/g,"").split("/");
	var reverse = x[1] + "/" + x[0] + "/" + x[2];
	return new Date(reverse).getTime();
}

var getTimeString = function(time) {
	var x = time.replace(/"/g,"").split("/");
	var reverse = x[1] + "/" + x[0] + "/" + x[2];
	return reverse;
}

var loadFKpis = function(callback) {
	google(googleSheetId, 14, function(kpis) {
		kpis.splice(0, 1);
		async.eachSeries(kpis, function(record, icallback) {
			var dbrecord = {
				model : "kpi",
				funderId: fundMap[record[0]].uuid,
				name : record[2],
				status : "active",
				projection : record[3],
				period : "" + periodMap[record[4]],
				periodStart : record[5].replace(/"/g,""),
				periodStartTime : getTime(record[5]),
				type : record[7].toLowerCase()
			};
			if (record[6]) {
				console.log("UUID", record[6]);
				var template = tempMap[record[6]];
				dbrecord.templateId = template.uuid;
				if (record[8]) {
					var row = template.fields.filter(function(x) {
						return x.name === record[8];
					})[0];
					if (row) {
						dbrecord.rowId = row.uuid;
					}
				}
				if (record[9]) {
					var row = template.fields.filter(function(x) {
						return x.name === record[9];
					})[0];
					if (row) {
						dbrecord.rowId2 = row.uuid;
					}
				}
			}
			kpiDb.saveDocument(dbrecord, function() {
				kpiMap[record[1]] = dbrecord;
				icallback();
			});
		}, callback);
	});
}

var loadFMaps = function(callback) {
	google(googleSheetId, 15, function(maps) {
		maps.splice(0, 1);
		async.eachSeries(maps, function(record, icallback) {
			var dbrecord = {
				"organisationId" : orgMap[record[2]].uuid,
				"funderId" : fundMap[record[0]].uuid,
				"templateId" : tempMap[record[1]].uuid,
				"mtemplateId" : tempMap[record[3]].uuid
			}
			var counter = 4;
			while (true) {
				if (record[counter] === undefined || record[counter] === "") {
					break;
				}
				var ftemplate = tempMap[record[1].split("|")[0]];
				var otemplate = tempMap[record[3].split("|")[0]];
				var ffield = ftemplate.fields.filter(function(x) {
					return x.name === record[counter];
				})[0];
				var ofield = otemplate.fields.filter(function(x) {
					return x.name === record[counter + 1];
				})[0];
				dbrecord[ffield.uuid] = {
					rowId: ofield.uuid
				};
				if (ffield.options) {
					var foptions = record[1].split("|")[1].split(",")
					var ooptions = record[3].split("|")[1].split(",");
					dbrecord[ffield.uuid].options = {};
					foptions.forEach(function(x, index) {
						var fRecord = ffield.options.filter(function(y) {
							return y.name === x;
						})[0];
						var oRecord = ofield.options.filter(function(y) {
							return y.name === ooptions[index];
						})[0];
						dbrecord[ffield.uuid].options[fRecord.uuid] = oRecord.uuid;
					});
				}
				counter+=2;
			}
			mtempDb.saveDocument(dbrecord, function() {
				icallback();
			});
		}, callback);
	});
}

var loadGraphs = function(callback) {
	google(googleSheetId, 16, function(graphs) {
		graphs.splice(0, 1);
		async.eachSeries(graphs, function(record, icallback) {
			var dbrecord = {
				"organisationId" : proMap[record[0]].organisationId,
				"programmeId" : actMap[record[0]].programmeId,
				"activityId" : actMap[record[0]].uuid,
				"name" : record[1],
				"description" : record[2],
				"gtype" : record[3],
				"type" : record[4]
			}
			if (dbrecord.type === "kpi") {
				dbrecord.kpiId = kpiMap[record[5]].uuid;
 			}
			else if (dbrecord.type === "record") {
				dbrecord.templateId = tempMap[record[5]].uuid;
				if (record[6]) {
					dbrecord.columnX = tempMap[record[5]].fields.filter(function(x) {
						return x.name === record[6]
					})[0].uuid;
				}
			}
			graphDb.saveDocument(dbrecord, function() {
				icallback();
			});
		}, callback);
	});
}

var googleSheetId = process.argv[2];
var loadData = function() {
	console.log("1");
	data(function() {
		console.log("2");
		loadFunders(function() {
			console.log("3");
			loadOrganisations(function() {
				console.log("4");
				loadProgrammes(function() {
					console.log("5");
					loadActivities(function() {
						console.log("6");
						loadFinancing(function() {
							console.log("7");
							loadTemplates(function() {
								console.log("8");
								loadKPIs(function() {
									console.log("9");
									loadRecords(function() {
										console.log("10");
										loadStories(function() {
											console.log("11");
											loadFTemplates(function() {
												console.log("12");
												loadFKpis(function() {
													console.log("13");
													loadFMaps(function() {
														console.log("4");
														loadGraphs(function() {
															console.log("DONE");
															process.exit(1);
														});
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
}

module.exports = loadData;