var xlsx = require("xlsx");
var data_workbook = xlsx.readFile('./data.xlsx');
var workbook = xlsx.readFile('./sim.xlsx');
var async = require("async");
var request = require("superagent");
var UUID = require("node-uuid");
var fs = require("fs");
var config = require("../config");
var mime = require("mime-types");
var secSv = require("../service/security");
var dataMap = {};

var token;
var fixed = false;
var passwords = {};

//db lookups
var userMap = {};
var fundMap = {};
var orgMap = {};
var proMap = {};
var actMap = {};
var tempMap = {};
var fieldMap = {};
var periodMap = {};
var countryMap = {};

//lookups
var outMap = {};
var out2Map = {};
var finMap = {};

var fundDb = require("../db/funders");
var userDb = require("../db/users");
var orgDb = require("../db/organisations");
var outDb = require("../db/outcomes");
var proDb = require("../db/programmes");
var actDb = require("../db/activities");
var kpiDb = require("../db/kpis");
var temDb = require("../db/templates");
var recDb = require("../db/records");
var stoDb = require("../db/stories");
var graDb = require("../db/grants");
var finDb = require("../db/financing");
var conDb = require("../db/contentTypes");
var dowDb = require("../db/downloads");
var userSv = require("../service/users");
var getDataFromSheet = require("./sheet");

var API_URL = config.server.apiUrl;

var loadOutcomes = function(callback) {
	outDb.findDocuments({}, function(err, docs) {
		docs.forEach(function(doc) {
			outMap[key(doc.name)] = doc;
		});
		callback();
	});
}

var loadFinancingTypes = function(callback) {
	finDb.findDocuments({}, function(err, docs) {
		docs.forEach(function(doc) {
			finMap[doc.name] = doc;
		});
		callback();
	});
}


var loadFieldTypes = function() {
	dataMap["fieldtype"].forEach(function(type, index) {
		fieldMap[type] = dataMap["fieldtypeId"][index];
	});
}

var loadPeriods = function() {
	dataMap["periods"].forEach(function(type, index) {
		periodMap[type] = dataMap["periodId"][index];
	});
}

var data = function(callback) {
	var data = getDataFromSheet(data_workbook.Sheets["data"]);
	var columns = ["outcomes", "periods", "kpitype", "fieldtype", "contentType", "financingType", "fieldtypeId", "periodId"]
	data.forEach(function(row) {
		row.forEach(function(col, index) {
			if (!dataMap[columns[index]]) {
				dataMap[columns[index]] = [];
			}
			dataMap[columns[index]].push(col);
		});
	});
	outMap = {};
	loadOutcomes(function() {
		loadFinancingTypes(function() {
			loadFieldTypes();
			loadCountries();
			loadPeriods();
			callback();
		});
	})
}

var loadCountries = function(callback) {
	var records = getDataFromSheet(data_workbook.Sheets["countries"]);
	records.forEach(function(record) {
		countryMap[record[1]] = record[0];
	});
}

var key = function(input) {
	return input ? value(input).toLowerCase().replace("-","") : "";
}

var value = function(input) {
	return input ? input.trim().replace(/\t/,"").replace(/\(/,"").replace(/\)/,"") : "";
}


var loadFunders = function(callback) {
	var data = getDataFromSheet(workbook.Sheets["Social Impact Metrics"]);
	fundMap = {};
	data.splice(0,1);
	data.forEach(function(x) {
		var fund = x[1] || x[0];
		if (!fundMap[key(fund)]) {
			fundMap[key(fund)] = {
				name: value(fund)
			};
		}
	});
	callback();
}

var loadOrganisations = function(callback) {
	var data = getDataFromSheet(workbook.Sheets["Social Impact Metrics"]);
	orgMap = {};
	data.splice(0,1);
	data.forEach(function(x) {
		var org = x[3];
		var split = org.split(";");
		split.forEach(function(one) {
			var outcome = x[5];
			var funder = x[1] || x[0];
			var financing = x[0];
			if (!orgMap[key(one)]) {
				orgMap[key(one)] = {
					name: value(one),
					mission: split.length === 1 ? value(x[4]) : null,
					outcomes: {},
					funders: {},
					financings: {}
				};
			}
			if (!orgMap[key(one)].funders[key(funder)]) {
				orgMap[key(one)].funders[key(funder)] = {
					name: value(funder),
					programme: split.length > 1 ? value(x[4]) : null
				};
			}
			if (!orgMap[key(one)].financings[key(financing)]) {
				orgMap[key(one)].financings[key(financing)] = {
					name: value(financing)
				};
			}
			if (!orgMap[key(one)].outcomes[key(outcome)]) {
				orgMap[key(one)].outcomes[key(outcome)] = {
					name: value(outcome),
					kpis: []
				};
				if (!out2Map[key(outcome)]) {
					out2Map[key(outcome)] = value(outcome);
				}
			}
			var metric = x[6];
			if (metric !== undefined && metric !== "n/a") {
				var kpi = {
					metric: metric,
					target: x[7 + (fixed ? 1 : 0)],
					impact: x[8 + (fixed ? 1 : 0)] || x[9 + (fixed ? 1 : 0)]
				};
				if (fixed) {
					kpi.description = x[7];
				}
				orgMap[key(one)].outcomes[key(outcome)].kpis.push(kpi);
			}
		});
	});
	callback();
}

var generateEmail = function(input) {
	return input.toLowerCase().replace(/;/g,"").replace(/,/g,"").replace(/&/g,"").replace(/ /g,"").substring(0, 20) + "-demo@marklabs.co";
}

var generatePassword = function(name, email) {
	return "12345";
}

var generateUser = function(input, callback) {
	var email = generateEmail(input);
	var query = userSv.searchQueryEmail(email);
	query.load = "true";
	userDb.findDocument(query, function(err, doc) {
		if (!doc) {
			var password = generatePassword(input, email);
			passwords[email] = password;
			var user = userSv.createUser(email, password);
			user.firstname = input;
			user.lastname = "Admin Account";
			user.load = "true";
			userDb.saveDocument(user, function() {
				userMap[key(input)] = user;
				callback(user);
			});
		}
		else {
			userMap[key(input)] = doc;
			callback(doc);
		}
	});
};

var createUsers = function(callback) {
	async.eachSeries(Object.keys(fundMap), function(funder, icallback) {
		generateUser(fundMap[funder].name, function(user) {
			fundDb.findDocument({name: funder.name, load: "true"}, function(err, doc) {
				if (doc) {
					fundMap[funder].uuid = doc.uuid;
					icallback();
				}
				else {
					var funderdb = userSv.createFunder(fundMap[funder].name, user.uuid);
					funderdb.load = "true";
					fundDb.saveDocument(funderdb, function(err, doc) {
						fundMap[funder].uuid = funderdb.uuid;
						fundMap[funder].user = user;
						icallback();
					});
				}
			});
		});
	}, function() {
		async.eachSeries(Object.keys(orgMap), function(org, icallback) {
			var orgDoc = orgMap[org];
			generateUser(orgDoc.name, function(user) {
				orgDb.findDocument({name: orgDoc.name, load: "true"}, function(err, doc) {
					if (doc) {
						orgMap[org].uuid = doc.uuid;
						icallback();
					}
					else {
						var organisation = userSv.createOrganisation(orgDoc.name, null, user.uuid);
						Object.keys(orgDoc.funders).forEach(function(fund) {
							var funder = fundMap[fund];
							organisation.funders.push({
								funderId: funder.uuid,
								status: "enabled"
							});
						})
						organisation.load = "true";
						organisation.mission = orgDoc.mission;
						orgDb.saveDocument(organisation, function(err, doc) {
							orgMap[org].uuid = organisation.uuid;
							orgMap[org].user = user;
							icallback();
						});
					}
				});
			});
		}, function() {
			callback();
		})
	});
}

var sendWithToken = function(path, body, ptoken, callback) {
	if (path.indexOf("?") === 0) {
		path = path + "&token=" + ptoken;
	}
	else {
		path = path + "?token=" + ptoken;
	}
    request.post(config.server.apiUrl + path)
		.send(body)
		.end(function(err, resp) {
			callback(err, resp.body);
		});
}

var getPassword = function(email) {
	return passwords[email];
}

var post = function(user, path, body, callback) {
	if (token) {
		sendWithToken(path, body, token, callback);
	}
	else {
		var login = {
			email: user.email,
			password: getPassword(user.email)
		};
		request.post(config.server.apiUrl + "/auth/login")
			.send(login)
			.end(function(err, resp) {
				token = resp.body.sessionId;
				sendWithToken(path, body, token, callback);
			})
	}
};


var isNumber = function(input)  {
	return !isNaN(parseFloat(input)) && isFinite(input);
}

var getNumber = function(input) {
  	return isNumber(input) ? parseFloat(input) : 0;
}

var getProjected = function(projected) {
	if (projected) {
		return getNumber(projected);
	}
	else {
		return 0;
	}
}

var getImpact = function(input) {
	return isNumber(input) ? parseFloat(input) : 0;
}

var loadTemplatesAndKPIS = function(orgDoc, activity, outcome, callback) {
	async.eachSeries(outcome.kpis, function(kpi, icallback) {
		var rowIdNum = UUID.v4();
		var rowIdDesc = UUID.v4();
		var templateRecord = {
			model : "template",
			organisationId : activity.organisationId,
			programmeId : activity.programmeId,
			activityId : activity.uuid,
			name : "Template for " + kpi.metric,
			fields: [
				{
					"uuid" : rowIdNum,
					"name" : "Count",
					"type" : "number",
				},
				{
					"uuid" : rowIdDesc,
					"name" : "Description",
					"type" : "textarea",
				}
			]
		}
		var kpidb = {
			organisationId : activity.organisationId,
			programmeId : activity.programmeId,
			activityId : activity.uuid,
			name: kpi.metric,
			status: "active",
			projected: getProjected(kpi.target),
			period: 360,
			periodfrom: new Date("01/01/2016").getTime(),
			type: "sum",
			rowId: rowIdNum
		};
		var recordDb;
		if (kpi.impact) {
			recordDb = {
				model : "record",
				organisationId : activity.organisationId,
				programmeId : activity.programmeId,
				activityId : activity.uuid,
				rows : [
					{
						"uuid" : rowIdNum,
						"name" : "Count",
						"type" : "number",
						"options" : [ ],
						"text" : getImpact(kpi.impact)
					},
					{
						"uuid" : rowIdDesc,
						"name" : "Description",
						"type" : "textarea",
						"options" : [ ],
						"text" : kpi.impact
					}
				],
				locationId : "GB",
			}
		}
		if (kpi.description) {
			kpidb.description = kpi.description;
		}
		post(orgDoc.user, "/organisations/admin/" + activity.organisationId + "/programmes/" + activity.programmeId + "/activities/" + activity.uuid + "/templates/", templateRecord, function(err, templateResult) {
			kpidb.templateId = templateResult.uuid;
			post(orgDoc.user, "/organisations/admin/" + activity.organisationId + "/programmes/" + activity.programmeId + "/activities/ " + activity.uuid + "/kpis", kpidb, function(err, kpiResult) {
				if (recordDb) {
					recordDb.templateId = templateResult.uuid;
					post(orgDoc.user, "/organisations/user/" + activity.organisationId + "/programmes/" + activity.programmeId + "/activities/ " + activity.uuid + "/records", recordDb, function(err, kpiResult) {
						icallback();
					});
				}
				else {
					icallback();
				}
			});
		});
 	}, function() {
		callback();
	});
}

var loadFinancings = function(organisation, callback) {
	Object.keys(organisation.financings, function(financing, icallback) {
		post(organisation.user, "/organisations/user/" + activity.organisationId + "/programmes/" + activity.programmeId + "/grants", grant, function(err, kpiResult) {
			icallback();
		});
	}, function() {
		callback();
	});
}

var loadProgrammes = function(callback) {
	async.eachSeries(Object.keys(orgMap), function(org, icallback) {
		token = null;
		var orgDoc = orgMap[org];
		async.eachSeries(Object.keys(orgDoc.outcomes), function(outcome, icallback2) {
			var programme = {
				organisationId: orgDoc.uuid,
				name: orgDoc.outcomes[outcome].programme || orgDoc.outcomes[outcome].name,
				outcome: outMap[key(outcome)]
			};
			post(orgDoc.user, "/organisations/admin/" + orgDoc.uuid + "/programmes/", programme, function(err, programmeresult) {
				orgDoc.outcomes[outcome].programmeId = programmeresult.uuid;
				var activity = {
					organisationId: orgDoc.uuid,
					programmId: programme.uuid,
					name: orgDoc.outcomes[outcome].name
				};
				post(orgDoc.user, "/organisations/admin/" + orgDoc.uuid + "/programmes/" + programmeresult.uuid + "/activities", activity, function(err, activityresult) {
					loadTemplatesAndKPIS(orgDoc, activityresult, orgDoc.outcomes[outcome], function() {
						loadFinancings(orgDoc, icallback2);
					});
				});
			});
		}, icallback);
	}, callback);
}

data(function() {
	loadFunders(function() {
		loadOrganisations(function() {
			createUsers(function() {
				loadProgrammes(function() {
					console.log("DONE");
					process.exit(1);
				});
			});
		});
	});
});


