var xlsx = require("xlsx");
var request = require("request");
var async = require("async");
var download = require("download");
var UUID = require("node-uuid");
var fs = require("fs");
var mime = require("mime-types");
var config = require("../config");
var google = require("../google.js");
var dataMap = {};


//db lookups
var userMap = {};
var fundMap = {};
var orgMap = {};
var proMap = {};
var actMap = {};
var tempMap = {};
var fieldMap = {};
var periodMap = {};
var countryMap = {};

//lookups
var outMap = {};
var finMap = {};

var fundDb = require("../db/funders");
var userDb = require("../db/users");
var orgDb = require("../db/organisations");
var secSv = require("../service/security");
var outDb = require("../db/outcomes");
var proDb = require("../db/programmes");
var actDb = require("../db/activities");
var fieldsDb = require("../db/fieldTypes")
var kpiDb = require("../db/kpis");
var temDb = require("../db/templates");
var recDb = require("../db/records");
var stoDb = require("../db/stories");
var graDb = require("../db/grants");
var finDb = require("../db/financingTypes");
var conDb = require("../db/contentTypes");
var userSv = require("../service/users");
var dowDb = require("../db/downloads");

var loadAndSave = function(db, key, map, record, icallback) {
	db.findDocument(record, function(err, doc) {
		if (!doc) {
			db.saveDocument(record, function() {
				if (map !== null && key !== null) {
					map[key] = record;
				}
				icallback();
			});

		}
		else {
			if (map !== null && key !== null) {
				map[key] = doc;
			}
			icallback();
		}
	});
}

var empty = function(name) {
	return name !== undefined && name !== "-" && name !== "";
}

var loadOutcomes = function(callback) {
	async.eachSeries(dataMap["outcomes"], function(name, icallback) {
		var index = dataMap["outcomes"].indexOf(name);
		if (empty(name)) {
			var dbrecord = {
				name: name,
				icon: dataMap["icons"][index]
			};
			loadAndSave(outDb, name, outMap, dbrecord, icallback);
		}
		else {
			icallback();
		}
	}, function() {
		callback();
	});
}

var loadFinancingTypes = function(callback) {
	async.eachSeries(dataMap["financingType"], function(name, icallback) {
		if (empty(name)) {
			var dbrecord = {
				uuid: name.toLowerCase(),
				name: name
			};
			loadAndSave(finDb, name, finMap, dbrecord, icallback);
		}
		else {
			icallback();
		}
	}, function() {
		callback();
	});
}

var loadContentType = function(callback) {
	async.eachSeries(dataMap["contentType"], function(content, icallback) {
		if (empty(content) && content.toLowerCase() !== "photolink") {
			var dbrecord = {uuid: content.toLowerCase(), name: content};
			loadAndSave(conDb, null, null, dbrecord, icallback);
		}
		else {
			icallback();
		}
	}, callback);
}

var loadFieldTypes = function(callback) {
	var index = 0;
	async.eachSeries(dataMap["fieldtype"], function(content, icallback) {
		if (empty(content)) {
			var dbrecord = {uuid: dataMap["fieldtypeId"][index], name: content};
			index++;
			loadAndSave(fieldsDb, null, null, dbrecord, icallback);
		}
		else {
			icallback();
		}
	}, callback);
}


var data = function(callback) {
	google(googleSheetId, 0, function(data) {
		var columns = ["outcomes", "periods", "kpitype", "fieldtype",  "contentType", "financingType", "fieldtypeId", "periodId", "icons"];
		data.forEach(function(row) {
			row.forEach(function(col, index) {
				if (!dataMap[columns[index]]) {
					dataMap[columns[index]] = [];
				}
				dataMap[columns[index]].push(col);
			});
		});
		columns.forEach(function(x) {
			dataMap[x].splice(0,1);
		})
		outMap = {};
		loadOutcomes(function() {
			loadFinancingTypes(function() {
				loadContentType(function() {
					loadFieldTypes(function() {
						callback();
					});
				});
			});
		});
	});
}

var createAdminUser = function(callback) {
	userDb.findDocument({email: config.admin.email}, function(err, doc) {
		if (!doc) {
			var user = userSv.createUser(config.admin.email, config.admin.password);
			user.admin = true;
			userDb.saveDocument(user, callback);
		}
		else {
			callback();
		}
	})
}

var googleSheetId = "1ebV-InHY6nEyt6U_c8OObYv6B9_xExPYoZS4IpMmWYw";

var loadData  = function(callback) {
	data(function() {
		if (config.admin.email && config.admin.password) {
			createAdminUser(function() {
				console.log("DONE DATA");
				if (!callback) process.exit(1);
				else callback();
			})
		}
		else {
			console.log("DONE DATA");
			if (!callback)  process.exit(1);
			else callback();
		}
	});
}

module.exports = loadData;