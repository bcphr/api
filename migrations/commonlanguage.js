var google = require("../google.js");
var fs = require("fs");

var omap = {};
var fmap = {};
var rows = [];

google('1y39msSXzRaQafjRrC5RDQAOwjcCtEVzdIKx8-TbT2nU', 1, function(omark) {
	google('1y39msSXzRaQafjRrC5RDQAOwjcCtEVzdIKx8-TbT2nU', 2, function(fmark) {
		var headers = JSON.parse(JSON.stringify(omark[0])).map(function(x, index) {
			if (index !== 0) {
				return x;
			}
		});
		omark.splice(0, 1);
		fmark.splice(0, 1);
		headers.forEach(function(x, lang_index) {
			omark.forEach(function(row, index) {
				omap[row[0]] = row.join(",");
			});
			fmark.forEach(function(row, index) {
				fmap[row[0]] = row.join(",");
			});
		});
		Object.keys(omap).forEach(function(key) {
			if (!fmap[key]) {
				//console.log("DONT HAVE", key);
				rows.push(omap[key].split(",").join("\t"));
			}
			else if (fmap[key] !== omap[key]) {
				//console.log("DIFFERNT", key);
			}
			else {
				rows.push(omap[key].split(",").join("\t"));
			}
		});
		Object.keys(fmap).forEach(function(key) {
			if (!omap[key]) {
				rows.push(fmap[key].split(",").join("\t"));
			}
			else if (omap[key] !== fmap[key]) {
				//console.log("DIFFERNT", key);
			}
			else {
				rows.push(fmap[key].split(",").join("\t"));
			}
		});
		fs.writeFileSync("./common.txt", rows.join("\n"));
	});
});