var loadData = require("./loadData");
var loadCountries = require("./loadCountries");
var loadDemo = require("./loadDemo");

loadData(function() {
	loadCountries(function() {
		loadDemo(function() {
			console.log("DONE");
			process.exit(1);
		})
	});
})
