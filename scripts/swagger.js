var fs = require("fs");
var striptags = require("striptags");

var data = JSON.parse(fs.readFileSync("../apidoc/api_data.json","UTF-8"));
var project = JSON.parse(fs.readFileSync("../apidoc/api_project.json","UTF-8"));

var host = (input) => {
	var prefix = "https://";
	var index = input.indexOf(prefix);
	if (index === -1) {
		prefix = "http://";
		index = input.indexOf(prefix);
	}
	var nextIndexOf = input.indexOf("/", index + prefix.length + 1);
	if (nextIndexOf === -1) {
		nextIndexOf = input.length;
	}
	return input.substring(index + prefix.length, nextIndexOf);
}

var basePath = (input) => {
	var prefix = "https://";
	var index = input.indexOf(prefix);
	if (index === -1) {
		prefix = "http://";
		index = input.indexOf(prefix);
	}
	var nextIndexOf = input.indexOf("/", index + prefix.length + 2);
	if (nextIndexOf === -1) {
		nextIndexOf = input.length;
	}
	return input.substring(nextIndexOf);
}

var bodies = {};

var parseUrl = (url) => {
	var index = 0;
	var result = {};
	while (true) {
		var colon = url.indexOf(":", index);
		if (colon === -1) {
			break;
		}
		var colonAfter = url.indexOf("/", colon + 1);
		if (colonAfter === -1) colonAfter = url.length;
		result[url.substring(colon, colonAfter)] =  "{" + url.substring(colon + 1, colonAfter) +"}";
		index = colon + 1;
	}
	Object.keys(result).forEach(function(key) {
		url = url.replace(key, result[key]);
	});
	return url;
}

var schemes = (input) => {
	if (input.indexOf("https") === 0) return ["https"];
	return ["http"];
}

var getLocation = (input) => {
	switch (input) {
		case "Query": return "query";
		case "Parameter": return "path";
		case "Body": return "body";
	}
	return undefined;
}

var getType = (input) => {
	switch (input.type || input.field) {
		case "String": return "string";
		case "String[]": return "array";
		case "Boolean": return "boolean";
		case "Number": return "number";
		case "Base64": return "string";
	}
	return undefined;
}


var getFormat = (input) => {
	switch (input.type) {
		case "Number": return "double";
	}
	return undefined;
}

var isPrimitive = (input) => {
	return input.indexOf(":") === -1;
}

var addParam = (type, input) => {
	var ltype = getLocation(type);
	if (ltype) {
		var result =  {
			"name": input.field,
	        "in": ltype,
	        "description": striptags(input.description),
	        "required": !input.optional,
	        "type": getType(input),
	        "format": getFormat(input)
		}
		if (input.type === "String[]") {
			result.items = {
				"type": "string"
			}
		}
		if (input.type && input.type.indexOf("Object") === 0) {
			var tokens = input.type.split(":");
			result.type = "#/definitions/" + getType(tokens[1]);
		}
		return result;
	}
	else {
		var firstTime = false;
		if (!bodies[type]) {
			bodies[type] = {
		      "type": "object",
		      "required": [],
		      "properties": {}
		    };
		    firstTime = true;
		}
		if (!input.optional) {
			bodies[type].required.push(input.field);
		}
		if (isPrimitive(input.field)) {
			bodies[type].properties[input.field] = {
				type: getType(input)
			}
		}
		else {
			var tokens = input.field.split(":");
			if (tokens.length === 3) {
				bodies[type].properties[tokens[2]] = {
    				"$ref": "#/definitions/" + tokens[1]
	  			}
			}
			else {
				bodies[type].properties[tokens[1]] = {
					type: getType(tokens[0])
				}
			}
		}
		if (firstTime) {
			return {
				"name": type,
		        "in": "body",
		        "schema": {
	    			"$ref": "#/definitions/" + type
	  			},
		        "description": striptags(input.description),
		        "required": !input.optional,
		        "format": getFormat(input)
			}
		}
		else {
			return null;
		}
	}
}

var getKey = (input) => {
	if (input.in === "path") {
		return "integration.request.path." + input.name;
	}
	else if (input.in === "query") {
		return "integration.request.querystring." + input.name;
	}
}

var getValue = (input) => {
	if (input.in === "path") {
		return "method.request.path." + input.name;
	}
	else if (input.in === "query") {
		return "method.request.querystring." + input.name;
	}
}

var result = {
	swagger: "2.0",
	info: {
		version: project.version,
		title: project.title || project.name,
		description: striptags(project.description),
	},
	host: host(project.url),
	basePath: basePath(project.url),
	schemes: schemes(project.url),
	consumes: [
		"application/json"
	],
	produces: [
		"application/json"
	],
	paths: {},
	definitions: {},
	securityDefinitions: {
		"api_key": {
			"type": "apiKey",
			"name": "x-api-key",
			"in": "header"
			}
	},
};

var dataMap = {};
data.forEach((x) => {
	var url = parseUrl(x.url);
	if (!dataMap[url]) {
		dataMap[url] = {};
	}
	dataMap[url][x.type] = {
		description: striptags(x.description),
		operationId: x.title,
		produces: ["application/json"],
		tags: [x.group],
		parameters: [],
		responses: {
			200: {
				description: "Successful Request"
			}
  		},
  		"x-amazon-apigateway-integration" : {
    		"type" : "http",
    		"uri" : project.url + url,
    		"httpMethod" : x.type.toUpperCase(),
    		"requestParameters" : {},
    		"responses": {
	            "default": {
	              "statusCode": "200"
	            }
          	}
		},
      	"security": [{
        	"api_key": []
      	}]
	}
	if (x.parameter) {
		Object.keys(x.parameter.fields).forEach((key) => {
			x.parameter.fields[key].forEach((param) => {
				var y = addParam(key, param);
				if (y) {
					if (dataMap[url][x.type].parameters.filter(function(x) {
						return x.in === "body";
					}).length === 0 || y.in !== "body") {
						dataMap[url][x.type].parameters.push(y);
					}
					var key3 = getKey(y);
					if (key3) {
						dataMap[url][x.type]["x-amazon-apigateway-integration"].requestParameters[key3] = getValue(y);
					}
				}
			});
		});
	}
	dataMap[url][x.type].parameters.push({
		"name": "x-access-token",
		"in": "header",
		"required": false,
		"type": "string"
    });
	dataMap[url][x.type]["x-amazon-apigateway-integration"].requestParameters["integration.request.header.x-access-token"] = "method.request.header.x-access-token";
});
result.paths = dataMap;
result.definitions = bodies;

fs.writeFileSync("./swagger.json", JSON.stringify(result));