var kpis = require("./kpis");

var fileToCheck ="/tmp/running";
var fs = require('fs');
var async = require("async");

var commands = process.argv[2].split(",");
console.log("RUNNING AT" + new Date()  + "WITH COMMANDS" + commands);

var func;
async.eachSeries(commands, function(command, callback) {
	console.log("Running", command);
	fs.exists(fileToCheck+command, function(exists) {
		if (!exists) {
			switch (command) {
				case "kpis": func = kpis; break;
			}
			if (func) {
				fs.writeFile(fileToCheck+command, "WORKING " + new Date(), function(err) {
					console.log("About to start", command, new Date());
					func(function() {
						console.log("Finished", command, new Date());
						fs.unlinkSync(fileToCheck+command);
						callback();
					});
				});
			}
			else {
				callback();
			}
		}
		else {
			console.log("ALREADY RUNNING", command);
			callback();
		}
	})
}, function() {
	console.log("DONE");
	process.exit(0);
});