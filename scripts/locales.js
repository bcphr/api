var google = require("../google.js");
var fs = require("fs");
var config = require("../config");
var async = require("async");

var map = ["web"];

var lmap = {
	"en": ["en-us", "en-gb"]
}

var getAppliedLangauges = function(input) {
	return lmap[input] || [input];
}

google(config.setup.GoogleLocaleFile, 0, function(country_rows) {
	var headers = JSON.parse(JSON.stringify(country_rows[0])).map(function(x, index) {
		if (index !== 0) {
			return x;
		}
	});
	country_rows.splice(0, 1);
	async.eachSeries(map, function(keyLang, callback) {
		var index = map.indexOf(keyLang);
		google(config.setup.GoogleLocaleFile, index + 1, function(field_rows) {
			google(config.setup.GoogleLocaleFile, 3, function(common_rows) {
				field_rows.splice(0, 1);
				common_rows.splice(0, 1);
				var languageMap = {};
				headers.forEach(function(x, lang_index) {
					if (lang_index !== 0) {
						getAppliedLangauges(x).forEach(function(lang) {
							if (!languageMap[lang]) {
								languageMap[lang] = {};
							}
							country_rows.forEach(function(country, index) {
								languageMap[lang][country[0]] = country[lang_index] || languageMap[lang][country[0]];
							});
							field_rows.forEach(function(row, index) {
								languageMap[lang][row[0]] = row[lang_index] || languageMap[lang][row[0]];
							});
							common_rows.forEach(function(row, index) {
								languageMap[lang][row[0]] = row[lang_index] || languageMap[lang][row[0]];
							});
						});
					}
				});
				Object.keys(languageMap).forEach(function(key) {
					filename = key + ".json";
					fs.writeFileSync("../../" + keyLang + "/public/locales/locale-" + filename.toLowerCase(), JSON.stringify(languageMap[key]));
				});
				callback();
			});
		});
	}, function() {
		console.log("DONE");
	});
});
