var codes = require("../codes");
var uuid = require("node-uuid");
var notupdateable = ["uuid", "_id", "created", "updated"];

exports.clean = function(returning) {
    if (Array.isArray(returning)) {
        returning.forEach(function(x) {
            exports.clean(x);
        })
    }
    else if (returning != null) {
        for (var x in returning) {
            if (Array.isArray(returning[x])) {
                exports.clean(returning[x]);
            }
        }
        delete returning._id;
        // delete returning.created;
        // delete returning.updated;
    }
}

exports.processDb = function(err, result, res, object) {
	if (!err && result)	 {
        var returning = JSON.parse(JSON.stringify(object || result || {}));
        exports.clean(returning);
		res.send(returning);
	}
	else {
        exports.saveError(err, result, res);
	}
}

var keysToUse = function(custom, normal) {
    var keys = custom || Object.keys(normal);
    return keys.filter(function(x) {
        return notupdateable.filter(function(y) {
            return x === y;
        }).length === 0;
    })
}

exports.saveError = function(err, message, res) {
    var uuid1 = uuid.v4();
    console.log("ERROR", uuid1);
    console.log("ERROR", err);
    console.log("ERROR", message);
    console.log("ERROR", uuid1);
    res.status(codes.DB_ERROR_CODE).send({error: uuid1});
}

exports.saveRecord = function(req, res, db, createkeys, updateablekeys, record, notify, input, idKey, validateFunction) {
	input = input || req.body;
    var validateResult;
    if (validateFunction) {
        validateResult = validateFunction(input);
    }
    if (!validateResult) {
        var key = idKey || "uuid";
        var id = req.params[key] || req.body[key];
        if (id) {
            db.findDocumentById(id, function(err, dbrecord) {
                if (!err && dbrecord) {
                    keysToUse(updateablekeys, input).forEach(function(x) {
                        if (input[x]) {
                            dbrecord[x] = input[x];
                        }
                    });
                    db.saveDocument(dbrecord, function(err, message) {
                        if (notify) updateSv.saveUpdate(req, "update", dbrecord);
                        exports.processDb(err, message, res, dbrecord);
                    });
                }
                else {
                    exports.saveError(err, dbrecord, res);
                }
            });
        }
        else {
            keysToUse(createkeys, input).forEach(function(x) {
                if (input[x]) {
                    record[x] = input[x];
                }
            });
            db.saveDocument(record, function(err, message) {
                if (notify) updateSv.saveUpdate(req, "create", record);
                exports.processDb(err, message, res, record);
            });
        }
    }
    else {
        exports.saveError(400, validateResult, res);
    }
}

exports.removeRecord = function(req, res, db, query, notify) {
    if (notify) {
        db.findDocument(query, function(err, oldrecord) {
            if (!err && message) {
                updateSv.saveUpdate(req, "remove", oldrecord);
                db.removeDocument(query, function(err, message) {
                    exports.processDb(err, message, res, {});
                });
            }
            else {
                exports.processDb(err, message, res);
            }
        })
    }
    else {
        db.removeDocument(query, function(err, message) {
            exports.processDb(err, message, res, {});
        });
    }
}