#downloads
db.downloads.createIndex( { uuid: 1 } );

#users
db.users.createIndex( { uuid: 1 } );
db.users.createIndex( { verifyCode: 1 } );
db.users.createIndex( { email: 1, password: 1 } );