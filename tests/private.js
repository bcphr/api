var should = require("chai").should(),
    expect = require("chai").expect,
    fs = require("fs"),
    conDb = require("../db/contentTypes"),
    fieldDb = require("../db/fieldTypes"),
    finDb = require("../db/financing"),
    outDb = require("../db/outcomes"),
    fileDb = require("../db/downloads"),
    userDb = require("../db/users"),
    apiTestServer = require("./apiTestServer");

describe('Private API', () => {
    before((done) => { //Before each test we empty the database
        console.log("BEFORE ALL");
        done();
    });

    beforeEach((done) => { //Before each test we empty the database
        userDb.dropCollection(() => {
            conDb.dropCollection(() => {
                fieldDb.dropCollection(() => {
                    finDb.dropCollection(() => {
                        outDb.dropCollection(() => {
                            fileDb.dropCollection(() => {
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

    afterEach((done) => { //Before each test we empty the database
        console.log("AFTER EACH");
        done();
    });

    after((done) => { //Before each test we empty the database
        console.log("AFTER ALL");
        done();
    });

    describe('UploadFile', () => {
        it('rejects when no name', (done) => {
            var input = {
                data: "AAAAA"
            };
            apiTestServer.userLogin(null, done, (err, token) => {
                apiTestServer.spost(done, "/private/upload", token, input, (err, resp) => {
                    done();
                }, 400);
            });
        });

        it('rejects when no data', (done) => {
            var input = {
                name: "AAAAA"
            };
            apiTestServer.userLogin(null, done, (err, token) => {
                apiTestServer.spost(done, "/private/upload", token, input, (err, resp) => {
                    done();
                }, 400);
            });
        });

        it('uploads a file', (done) => {
            var input = {
                name: "AAAAA.txt",
                data: "BBBBBBB"
            };
            apiTestServer.userLogin(null, done, (err, token) => {
                apiTestServer.spost(done, "/private/upload", token, input, (err, resp) => {
                    resp.should.have.property('uuid');
                    var postfixindex = input.name.lastIndexOf(".");
                    var postfix;
                    if (postfixindex !== -1) {
                        postfix = input.name.substring(postfixindex);
                    }
                    stats = fs.lstatSync("./tmp/" + resp.uuid + postfix);
                    expect(stats.isFile()).to.be.true;
                    fileDb.findDocument({uuid: resp.uuid}, (err, doc) => {
                        doc.should.not.be.empty;
                        doc.should.have.property('postfix').and.be.equal(postfix);
                        doc.should.have.property('name').and.be.equals(input.name);
                        done();
                    });
                });
            });
        });
    });

    describe('DownloadFile', () => {
        it('views an existing file', (done) => {
            var file = {
                "uuid" : "AAAAA",
                "name" : "m1.jpg",
                "format" : "image/jpeg",
                "postfix" : ".jpg"
            };
            try {
                fs.unlinkSync("./tmp/AAAAA" + file.postfix);
            }
            catch (e) {}
            fs.writeFileSync("./tmp/AAAAA" + file.postfix, "ABCDEF");
            fileDb.saveDocument(file, ()=>{
                apiTestServer.userLogin(null, done, (err, token) => {
                    apiTestServer.sdownload(done, "/private/download/" + file.uuid, token, (err, resp) => {
                        resp.should.not.be.empty;
                        done();
                    });
                });
            });
        });

        it('fails on a nonexisting file', (done) => {
            var file = {
                "uuid" : "AAAAA",
                "name" : "m1.jpg",
                "format" : "image/jpeg",
                "postfix" : ".jpg"
            };
            try {
                fs.unlinkSync("./tmp/AAAAA" + file.postfix);
            }
            catch (e) {}
            fileDb.saveDocument(file, ()=>{
                apiTestServer.userLogin(null, done, (err, token) => {
                    apiTestServer.sdownload(done, "/private/download/" + file.uuid, token, (err, resp) => {
                        done();
                    }, 400);
                });
            });
        });

        it('fails on a nonexisting uuid', (done) => {
            var file = {
                "uuid" : "AAAAA",
                "name" : "m1.jpg",
                "format" : "image/jpeg",
                "postfix" : ".jpg"
            };
            try {
                fs.unlinkSync("./tmp/AAAAA" + file.postfix);
            }
            catch (e) {}
            apiTestServer.userLogin(null, done, (err, token) => {
                apiTestServer.sdownload(done, "/private/download/" + file.uuid, token, (err, resp) => {
                    done();
                }, 400);
            });
        });
    });

    describe('ListContentTypes', () => {
        var object = {
            uuid: "AAAA"
        }
        it('return objects from db', (done) => {
            conDb.saveDocument(object, () => {
                apiTestServer.userLogin(null, done, (err, token) => {
                    apiTestServer.sget(done, "/private/contentTypes", token, (err, resp) => {
                        resp.should.not.be.empty;
                        resp.should.have.length.within(1,1);
                        resp[0].should.have.property('uuid').and.be.equal(object.uuid);
                        done();
                    });
                });
            });
        });
    });

    describe('ListFieldTypes', () => {
        var object = {
            uuid: "AAAABBB"
        }
        it('return objects from db', (done) => {
            fieldDb.saveDocument(object, () => {
                apiTestServer.userLogin(null, done, (err, token) => {
                    apiTestServer.sget(done, "/private/fieldTypes", token, (err, resp) => {
                        resp.should.not.be.empty;
                        resp.should.have.length.within(1,1);
                        resp[0].should.have.property('uuid').and.be.equal(object.uuid);
                        done();
                    });
                });
            });
        });
    });

    describe('ListFinancingTypes', () => {
        var object = {
            uuid: "AAAABBBCCCC"
        }
        it('return objects from db', (done) => {
            finDb.saveDocument(object, () => {
                apiTestServer.userLogin(null, done, (err, token) => {
                    apiTestServer.sget(done, "/private/financingTypes", token, (err, resp) => {
                        resp.should.not.be.empty;
                        resp.should.have.length.within(1,1);
                        resp[0].should.have.property('uuid').and.be.equal(object.uuid);
                        done();
                    });
                });
            });
        });
    });

    describe('ListOutcomes', () => {
        var object = {
            uuid: "AAAABBBCCCCFFFF"
        }
        it('return objects from db', (done) => {
            outDb.saveDocument(object, () => {
                apiTestServer.userLogin(null, done, (err, token) => {
                    apiTestServer.sget(done, "/private/outcomes", token, (err, resp) => {
                        resp.should.not.be.empty;
                        resp.should.have.length.within(1,1);
                        resp[0].should.have.property('uuid').and.be.equal(object.uuid);
                        done();
                    });
                });
            });
        });
    });
});
