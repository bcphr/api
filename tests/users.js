var should = require("chai").should(),
    userDb = require("../db/users"),
    userSv = require("../service/users"),
    secSv = require("../service/security"),
    config = require("../config"),
    apiTestServer = require("./apiTestServer");

describe('Users API', () => {
    before((done) => { //Before each test we empty the database
        done();
    });

    beforeEach((done) => { //Before each test we empty the database
        userDb.dropCollection(() => {
            done();
        });
    });

    afterEach((done) => { //Before each test we empty the database
        console.log("AFTER EACH");
        done();
    });

    after((done) => { //Before each test we empty the database
        console.log("AFTER ALL");
        done();
    });

    describe('GetCurrentUser', () => {
        it('returns the current user', (done) => {
            var user = {
                "field1": "aaa",
                "email": "aaa@marklabs.co",
                "password": "bbb",
                "firstname": "name1",
                "surname": "name1",
                "preferences": { "pref1": "pref4"},
                "image": "image1",
            }
            apiTestServer.userLogin(user, done, (err, token) => {
                apiTestServer
                    .sget(done, "/users", token, function(err, res) {
                        res.user.should.have.property('email').and.be.equal(user.email);
                        res.user.should.have.property('uuid')
                        res.user.should.not.have.property('password');
                        res.user.should.have.property('firstname');
                        res.user.should.have.property('surname');
                        res.user.should.not.have.property('field1');
                        res.user.should.have.property('preferences');
                        res.user.should.have.property('image');
                        done();
                    });
            });
        });
    });

    describe('CheckEmailExists', () => {
        it('returns email if its in the database', (done) => {
            var user = {
                "email": "aaa+BBBB@marklabs.co",
                "password": "bbb",
            }
            apiTestServer.userLogin(user, done, (err, token) => {
                apiTestServer
                    .sget(done, "/users/exists/" + user.email + "/", token, function(err, res) {
                        res.should.have.property('email').and.be.equal(user.email);
                        done();
                    });
            });
        });

        it('returns 404 if email not in the database', (done) => {
            var user = {
                "email": "aaa+BBBB@marklabs.co",
                "password": "bbb",
            }
            apiTestServer.userLogin(user, done, (err, token) => {
                apiTestServer
                    .sget(done, "/users/exists/" + user.email + ("AAA") + "/", token, function(err, res) {
                        res.should.not.have.property('email').and.be.equal(user.email);
                        done();
                    }, 200);
            });
        });
    });

    describe('GetUser', () => {
        var user = {
            "field1": "aaa",
            "email": "aaa@marklabs.co",
            "password": "bbb",
            "uuid": "12345",
            "firstname": "name1",
            "surname": "name1",
            "preferences": { "pref1": "pref4"},
            "image": "image1",
        };

        it('returns the name', (done) => {
            apiTestServer.userLogin(user, done, (err, token) => {
                apiTestServer
                    .sget(done, `/users/${user.uuid}?filter=name`, token, function(err, res) {
                        res.should.have.property('email').and.be.equal(user.email);;
                        res.should.not.have.property('uuid')
                        res.should.not.have.property('password');
                        res.should.have.property('firstname').and.be.equal(user.firstname);
                        res.should.have.property('surname').and.be.equal(user.surname);
                        res.should.not.have.property('field1');
                        res.should.not.have.property('preferences');
                        res.should.not.have.property('image');
                        done();
                    });
            });
        });

        it('returns the image', (done) => {
            apiTestServer.userLogin(user, done, (err, token) => {
                apiTestServer
                    .sget(done, `/users/${user.uuid}?filter=image`, token, function(err, res) {
                        res.should.not.have.property('email');
                        res.should.not.have.property('uuid')
                        res.should.not.have.property('password');
                        res.should.not.have.property('firstname');
                        res.should.not.have.property('surname');
                        res.should.not.have.property('field1');
                        res.should.not.have.property('preferences');
                        res.should.have.property('image').and.be.equal(user.image);;
                        done();
                    });
            });
        });
    });

    describe('LogoutUser', () => {
        it('logs the user out', (done) => {
            apiTestServer.userLogin(null, done, (err, token) => {
                apiTestServer
                    .spost(done, "/users/logout", token, {}, function(err, res) {
                        done();
                    });
            });
        });
    });


    describe('RegisterUser', () => {
        var euser = {
            uuid: "ABCDEF",
            email: "qpr@email.com",
            preferences: {

            }
        }

        var input = {
            email: euser.email,
            password: "12345"
        }

        it('fails on existing user', (done) => {
            //creates the user and the funder
            userDb.saveDocument(euser, () => {
                apiTestServer
                    .post(done, `/auth/register/`, input, function(err, res) {
                        done();
                    }, 400);

            });
        });


        it('registers a user', (done) => {
            //creates the user and the funder
            var input2 = JSON.parse(JSON.stringify(input));
            apiTestServer
                .post(done, `/auth/register/`, input2, function(err, res) {
                    userDb.findDocument(userSv.searchQueryEmail(input2.email), (err, user) => {
                        user.should.have.property('uuid');
                        done();
                    });
                });
        });

        it('fails on no password', (done) => {
            //creates the user and the funder
            var input2 = JSON.parse(JSON.stringify(input));
            delete input2.password;
            apiTestServer
                .post(done, `/auth/register/`, input2, function(err, res) {
                    done();
                }, 400);
        });



        it('fails on existing user', (done) => {
            //creates the user and the funder
            var input2 = JSON.parse(JSON.stringify(input));
            delete input2.email;
            apiTestServer
                .post(done, `/auth/register/`, input2, function(err, res) {
                    done();
                }, 400);
        });
    });

    describe('VerifyUser', () => {
        var input = {
            email: "abc@gmail.com",
            password: "12345"
        }

        it('verifies a user', (done) => {
            //creates the user and the funder
            var input2 = JSON.parse(JSON.stringify(input));
            apiTestServer
                .post(done, `/auth/register/`, input2, function(err, res) {
                    userDb.findDocument(userSv.searchQueryEmail(input2.email), (err, user) => {
                        user.should.have.property('uuid');
                        user.should.have.property('verified').and.be.false;
                        apiTestServer
                            .post(done, `/auth/verify/${secSv.encrypt(user.verifyCode)}`, {}, function(err, res) {
                                userDb.findDocumentById(user.uuid, (err, newuser) => {
                                    newuser.should.have.property('verified').and.be.true;
                                    done();
                                });
                        });
                    });
                });
        });

        it('fails verifies bad code', (done) => {
            //creates the user and the funder
            var input2 = JSON.parse(JSON.stringify(input));
            apiTestServer
                .post(done, `/auth/register/`, input2, function(err, res) {
                    userDb.findDocument(userSv.searchQueryEmail(input2.email), (err, user) => {
                        user.should.have.property('uuid');
                        user.should.have.property('verified').and.be.false;
                        apiTestServer
                            .post(done, `/auth/verify/${secSv.encrypt(user.verifyCode)}AAAA`, {}, function(err, res) {
                                done();
                        }, 400);
                    });
                });
        });
    });

    describe('ValidatePassword', () => {
        var password = "12345";
        var user = {
            uuid: "ABCDEF",
            email: "qpr@email.com",
            password: password,
            preferences: {

            }
        }

        it('confirms the same password', (done) => {
            apiTestServer.userLogin(user, done, (err, token) => {
                var input = {
                    password : password
                }
                apiTestServer
                    .spost(done, "/auth/password/?requester=funder", token , input, (err, res) => {
                        done();
                    });
            });
        });

        it('rejects a different password', (done) => {
            apiTestServer.userLogin(user, done, (err, token) => {
                var input = {
                    password : password + "AAAA"
                }
                apiTestServer
                    .spost(done, "/auth/password/?requester=funder", token, input, (err, res) => {
                        done();
                    }, 403);
            });
        });

        it('rejects when no password is given password', (done) => {
            apiTestServer.userLogin(user, done, (err, token) => {
                var input = {
                    email : user.email
                }
                apiTestServer
                    .spost(done, "/auth/password/?requester=funder", token , input, (err, res) => {
                        done();
                    }, 400);
            });
        });
    });

    describe('LoginUser', () => {
        var password = "12345";
        var user = {
            uuid: "ABCDEF",
            email: "qpr@email.com",
            password: secSv.encrypt(password),
            preferences: {

            }
        }

        it('fails when email is not provided', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/login/?requester=funder", {password: password}, (err, res) => {
                        done();
                    }, 400);
            });
        });

        it('fails when password is not provided', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/login/?requester=funder", {email: user.email}, (err, res) => {
                        done();
                    }, 400);
            });
        });

        it('fails when both email and password is not provided', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/login/?requester=funder", {}, (err, res) => {
                        done();
                    }, 400);

            });
        });

        it('validates correct details', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/login/?requester=funder", {email: user.email, password: password}, (err, res) => {
                        done();
                    });

            });
        });

        it('fails when password is wrong', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/login/?requester=funder", {email: user.email, password: "AAAAA" + user.password}, (err, res) => {
                        done();
                    }, 404);

            });
        });

        it('fails when email is wrong', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/login/?requester=funder", {email: user.email + "AAAA", password: password}, (err, res) => {
                        done();
                    }, 404);
            });

        });

        it('works with pluses', (done) => {
            user.email = "aaa+1234@gmail.com";
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/login/?requester=funder", {email: user.email, password: password}, (err, res) => {
                        done();
                    });
            });
        });

        it('works with capitals', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/login/?requester=funder", {email: user.email.toUpperCase(), password: password}, (err, res) => {
                        done();
                    });
            });
        });
    });


    describe('UpdateUser', () => {
        var password = "12345";
        var user = {
            uuid: "ABCDEF",
            email: "qpr@email.com",
            password: password,
            preferences: {

            }
        }

        it('updates the current user', (done) => {
            var update = {
                firstname: "Hello",
                goodbye: "Goodbye"
            };
            apiTestServer.userLogin(user, done, (err, token) => {
                apiTestServer
                    .spost(done, "/users/?requester=funder", token, update, (err, res) => {
                        userDb.findDocument({email: user.email}, (err, dbuser) => {
                            dbuser.should.have.property('firstname').and.be.equal(update.firstname);
                            dbuser.should.not.have.property('goodbye');
                            done();
                        });
                    });
            });
        });
    });

    describe('SendResetLink', () => {
        var password = "12345";
        var user = {
            uuid: "ABCDEF",
            email: "qpr@email.com",
            password: secSv.encrypt(password),
            preferences: {

            }
        }

        it('fails when you dont send an email', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/users/sendReset", {}, (err, res) => {
                        done();
                    }, 400);
            });
        });

        it('fails when you send an unknown email', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/sendReset", {email: user.email +"AAAA"}, (err, res) => {
                        done();
                    }, 404);
            });
        });

        it('doesnt fail when you send a known email', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/sendReset", { email: user.email }, (err, res) => {
                        userDb.findDocument({email: user.email}, (err, dbuser) => {
                            dbuser.should.have.property('verifyCode');
                            done();
                        });
                    });
            });
        });
    });

    describe('ValidateResetLink', () => {
        var password = "12345";
        var user = {
            uuid: "ABCDEF",
            email: "qpr@email.com",
            password: secSv.encrypt(password),
            preferences: {

            }
        }

        it('works with a valid reset link', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/sendReset", { email: user.email }, (err, res) => {
                        userDb.findDocument({ email: user.email }, (err, dbuser) => {
                            apiTestServer.get(done, "/auth/reset/" + secSv.encrypt(dbuser.verifyCode) + "/?requester=funder", (err, res) => {
                                done();
                            });
                        });
                    });
            });
        });

        it('fails with an invalid reset link', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/sendReset", { email: user.email }, (err, res) => {
                        userDb.findDocument({ email: user.email }, (err, dbuser) => {
                            apiTestServer.get(done, "/auth/reset/" + secSv.encrypt(dbuser.verifyCode + "AAAA") + "/?requester=funder", (err, res) => {
                                done();
                            }, 400);
                        });
                    });
            });
        });

    });

    describe('ResetPassword', () => {
        var password = "12345";
        var user = {
            uuid: "ABCDEF",
            email: "qpr@email.com",
            password: secSv.encrypt(password),
            preferences: {

            }
        }
        var input = {
            password: password + "AAAAA"
        }

        it('fails if bad code', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/sendReset", { email: user.email }, (err, res) => {
                        userDb.findDocument({ email: user.email }, (err, dbuser) => {
                            apiTestServer.post(done, "/auth/reset/" + secSv.encrypt(dbuser.verifyCode + "AAAA") + "/?requester=funder", input, (err, res) => {
                                done();
                            }, 400);
                        });
                    });
            });
        });

        it('fails if password not provided', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/sendReset", { email: user.email }, (err, res) => {
                        userDb.findDocument({ email: user.email }, (err, dbuser) => {
                            apiTestServer.post(done, "/auth/reset/" + secSv.encrypt(dbuser.verifyCode) + "/?requester=funder", {}, (err, res) => {
                                done();
                            }, 400);
                        });
                    });
            });
        });

        it('can still use old password if didnt reset', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/sendReset", { email: user.email }, (err, res) => {
                        userDb.findDocument({ email: user.email }, (err, dbuser) => {
                            apiTestServer
                                .post(done, "/auth/login/?requester=funder", {email: user.email, password: password}, (err, res) => {
                                    done();
                                }, 200);
                        });
                    });
            });
        });

        it('updates the password', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/sendReset", { email: user.email }, (err, res) => {
                        userDb.findDocument({ email: user.email }, (err, dbuser) => {
                            apiTestServer.post(done, "/auth/reset/" + secSv.encrypt(dbuser.verifyCode) + "/?requester=funder", input, (err, res) => {
                                userDb.findDocument({uuid: user.uuid}, (err, dbuser) => {
                                    dbuser.should.have.property('password').and.be.equal(secSv.encrypt(input.password));
                                    dbuser.should.not.have.property('verifyCode');
                                    done();
                                });
                            });
                        });
                    });
            });
        });

        it('cant use old password after update', (done) => {
            userDb.saveDocument(user, () => {
                apiTestServer
                    .post(done, "/auth/sendReset", { email: user.email }, (err, res) => {
                        userDb.findDocument({ email: user.email }, (err, dbuser) => {
                            apiTestServer.post(done, "/auth/reset/" + secSv.encrypt(dbuser.verifyCode) + "/?requester=funder", input, (err, res) => {
                                apiTestServer
                                    .post(done, "/auth/login/?requester=funder", {email: user.email, password: password}, (err, res) => {
                                        done();
                                    }, 404);
                            });
                        });
                    });
            });
        });
    });
});
