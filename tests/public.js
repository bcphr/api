var should = require("chai").should(),
    fileDb = require("../db/downloads"),
    apiTestServer = require("./apiTestServer"),
    locDb = require("../db/locations");
    orgDb = require("../db/organisations"),
    fs = require("fs"),
    secSv = require("../service/security");

describe('Public API', () => {
    before((done) => { //Before each test we empty the database
        console.log("BEFORE ALL");
        done();
    });

    beforeEach((done) => { //Before each test we empty the database
        fileDb.dropCollection(() => {
            locDb.dropCollection(() => {
                orgDb.dropCollection(() => {
                    done();
                });
            });
        });
    });

    afterEach((done) => { //Before each test we empty the database
        console.log("AFTER EACH");
        done();
    });

    after((done) => { //Before each test we empty the database
        console.log("AFTER ALL");
        done();
    });

    describe('ViewFile', () => {
        it('views an existing file', (done) => {
            var file = {
                "uuid" : "AAAAA",
                "name" : "m1.jpg",
                "format" : "image/jpeg",
                "postfix" : ".jpg"
            };
            if (fs.exists("./tmp/AAAAA" + file.postfix)) {
                fs.unlink("./tmp/AAAAA" + file.postfix);
            }
            fs.writeFileSync("./tmp/AAAAA" + file.postfix, "ABCDEF");
            fileDb.saveDocument(file, ()=>{
                apiTestServer.get(done, "/pub/view/" + file.uuid, (err, resp) => {
                    resp.should.not.be.empty;
                    fs.unlink("./tmp/AAAAA" + file.postfix);
                    done();
                });
            });
        });

        it('fails on a nonexisting file', (done) => {
            var file = {
                "uuid" : "AAAAA",
                "name" : "m1.jpg",
                "format" : "image/jpeg",
                "postfix" : ".jpg"
            };
            fileDb.saveDocument(file, ()=>{
                apiTestServer.get(done, "/pub/view/" + file.uuid, (err, resp) => {
                    done();
                }, 400);
            });
        });

        it('fails on a nonexisting uuid', (done) => {
            var file = {
                "uuid" : "AAAAA",
                "name" : "m1.jpg",
                "format" : "image/jpeg",
                "postfix" : ".jpg"
            };
            apiTestServer.get(done, "/pub/view/" + file.uuid, (err, resp) => {
                done();
            }, 400);
        });
    });

    describe('GetPublicOrganisation', () => {
        var org = {
            uuid: "123455",
            authorisations: [],
            admins: [],
            users: []
        };
        it('returns an existing organisation', (done) => {
            orgDb.saveDocument(org, () => {
                apiTestServer.get(done, `/pub/org/${secSv.encrypt(org.uuid)}`, (err, resp) => {
                    resp.should.have.property('uuid').and.be.equals(org.uuid);
                    done();
                });
            });
        });

        it('fails on a non-existing organisation', (done) => {
            apiTestServer.get(done, `/pub/org/${secSv.encrypt(org.uuid)}`, (err, resp) => {
                done();
            }, 400);
        });
    });

    describe('ListLocations', () => {
        var loc1 = {

        };
        var loc2 = {

        };
        it('returns locations', (done) => {
            locDb.saveDocuments([loc1, loc2], () => {
                apiTestServer.get(done, "/pub/locations/", (err, resp) => {
                    resp.should.not.be.empty;
                    resp.should.have.length.within(2,2);
                    done();
                });
            });
        });
    });
});
