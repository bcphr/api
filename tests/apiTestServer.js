var supertest = require("supertest");
var config = require("../config");
var api = supertest(config.server.apiUrl);
var db = require("../db/users");
var secSv = require("../service/security")

var HEADER_TOKEN = "x-access-token";

exports.createUser = function(userToCreate, callback) {
    var userDb = userToCreate ? userToCreate : {
        email: "aaaa@marklabs.co",
        password: "bbb",
        preferences: {

        }
    };
    if (!userDb.preferences) {
        userDb.preferences = {};
    }
    var userEncrypt = JSON.parse(JSON.stringify(userDb));
    userEncrypt.password = secSv.encrypt(userEncrypt.password);
    db.saveDocument(userEncrypt, function(err, resp) {
        userDb.uuid = userEncrypt.uuid;
        callback(userDb);
    });
}

var createAdmin = function(callback) {
    var user = {
        email: config.admin.email,
        password: secSv.encrypt(config.admin.password),
        admin: true,
        preferences: {

        }
    };
    db.saveDocument(user, function(err, resp) {
        callback();
    });
}

exports.get = function(done, url, callback, expectCode) {
    api.get(url)
        .expect(expectCode || 200)
        .end(function(err, res) {
            if (err) done(err);
            else callback(err, res.body);
        });
}

exports.post = function(done, url, body, callback, expectCode) {
    api.post(url)
        .send(body)
        .expect(expectCode || 200)
        .end(function(err, res) {
            if (err) done(err);
            else callback(err, res.body);
        });
}

exports.spost = function(done, url, token, body, callback, expectCode) {
	return api
        .post(url)
        .set(HEADER_TOKEN, token)
        .expect(expectCode || 200)
        .expect('Content-Type', /json/)
        .send(body)
        .end((err, res) => {
            if (err) done(err);
            else callback(err, res.body);
        });
}

exports.sdelete = function(done, url, token, callback, expectCode) {
    return api
        .delete(url)
        .set(HEADER_TOKEN, token)
        .expect(expectCode || 200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
            if (err) done(err);
            else callback(err, res.body);
        });
}

exports.sget = function(done, url, token, callback, expectCode) {
    return api
        .get(url)
        .set(HEADER_TOKEN, token)
        .expect(expectCode || 200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
            if (err) done(err)
            else callback(err, res.body);
        });
}


exports.sdownload = function(done, url, token, callback, expectCode) {
    return api
        .get(url)
        .set(HEADER_TOKEN, token)
        .expect(expectCode || 200)
        .end((err, res) => {
            if (err) done(err)
            else callback(err, res.body);
        });
}

exports.adminLogin = function(done, callback) {
    createAdmin(() => {
        var body =  {"email": config.admin.email, "password": config.admin.password};
        exports.post(done, "/auth/login/", body, (err, resp) => {
            if (err) done(err);
            else {
                callback(err, resp.token);
            }
        });
    });
}

exports.clone = function(input) {
    var result = JSON.parse(JSON.stringify(input));
    delete result._id;
    return result;
}

exports.userLogin = function(userToCreate, done, callback) {
    exports.createUser(userToCreate, (user) => {
        var body =  {"email": user.email, "password": user.password};
        exports.post(done, "/auth/login/", body, (err, resp) => {
            if (!resp.token) done(err);
            else {
                callback(err, resp.token);
            }
        });
    });
}