var APP_NAME = "APP_TEMPLATE";
var DOMAIN_NAME = "marklabs.co";

var config = {

    awsConfig: {
        accessKeyId: process.env[APP_NAME + "_AWSKEY"],
        secretAccessKey:  process.env[APP_NAME + "_AWSSECRET"],
        region:  process.env[APP_NAME + "_REGION"],
        aws_enabled: process.env[APP_NAME + "_AWS_ENABLED"],
        uploadbucket:  process.env[APP_NAME + "_UPLOAD_BUCKET"],
    },

    intercom: {
        secure: "1",
        app: "1",
        appId: "1",
    },

    server: {
        domain: DOMAIN_NAME,
        baseDir: process.env[APP_NAME + "_BASE_DIR"] || "/Users/samirseetal/code/fw/api/",
        webUrl: process.env[APP_NAME + "_WEB_URL"] || "http://localhost:3001",
        apiUrl: process.env[APP_NAME + "_API_URL"] || "http://localhost:3000",
        port: process.env[APP_NAME + "_PORT"] || 3000,
        secret: "SECRETMANALWAYSWORKS",
        tokenExpiry: 1440,
        log: true,
    },

    setup: {
        GoogleLocaleFile: '1y39msSXzRaQafjRrC5RDQAOwjcCtEVzdIKx8-TbT2nU'
    },

    db: {
        name: process.env[APP_NAME + "_DB_NAME"] || APP_NAME,
        port: process.env[APP_NAME + "_DB_PORT"] || "27017",
        host: process.env[APP_NAME + "_DB_HOST"] || "localhost"
    },

    admin: {
        email: "admin@" + DOMAIN_NAME,
        password: "AAAAA"
    },
};

if (process.env.API_ENV === "test") {
    config.awsConfig.aws_enabled = "false";
    config.db.name = config.db.name + "test";
    config.server.port = "13000";
    config.server.apiUrl = "http://localhost:" + config.server.port;
}

module.exports = config;
