function Db() {
	var uuid = require("node-uuid");
	var MongoClient = require("mongodb").MongoClient;
	var async = require("async");
	var config = require("./config");

	//DB STUFF
	var url = "mongodb://" + config.db.host + ":" + config.db.port + "/" + config.db.name;
	var db;
	var tablename;

	this.init = function(name) {
		tablename = name;
	};
	// Use connect method to connect to the Server

	this.close = function() {
		if (db) {
			db.close();
			db = null;
		}
	};

	this.createDocument = function() {
		var doc = {};
		doc.uuid = uuid.v4();
		doc._id = uuid.v4();
		doc.created = Date.now();
		doc.updated = doc.created;
		return doc;
	};

	this.saveUpdatedDocument = function(document, callback) {
		document.updated = Date.now();
		this.saveDocument(document, callback);
	};

	var connectToDb = function(callback) {
		if (db) {
			callback(db);
		}
		else {
			MongoClient.connect(url, function(err, d) {
				db = d;
				callback(db);
			});
		}
	}

	var saveDocument = function(document, callback) {
		connectToDb(function(db) {
			var collection = db.collection(tablename);
			if (document.uuid === undefined) {
				document.uuid = uuid.v4();
			}
			if (document._id === undefined) {
				document._id = uuid.v4();
			}
			if (document.created === undefined) {
				document.created = Date.now();
			}
			else {
				document.updated = Date.now();
			}
			collection.save(document, function(errdb, result) {
				if (callback) callback(errdb, result);
			});
		});
	}

	this.saveDocument = function(document, callback) {
		saveDocument(document, callback);
	};

	this.saveDocuments = function(documents, callback) {
		async.eachSeries(documents, function(document, icallback) {
			saveDocument(document,icallback);
		}, function(err) {
			if (callback) callback(err);
		});
	};

	this.removeDocument = function(document, callback) {
		connectToDb(function(db) {
			var collection = db.collection(tablename);
			collection.deleteMany(document, function(errdb, result) {
				if (callback) callback(errdb, result);
			});
		});
	};

	this.removeDocumentById = function(uuid, callback) {
		this.removeDocument({uuid: uuid}, callback);
	};

	this.updateDocument = function(query, update, options, callback) {
		connectToDb(function(db) {
			var collection = db.collection(tablename);
			collection.update(query, update, options, function(errdb, result) {
				if (callback) callback(errdb, result);
			});
		});
	};

	this.findDocuments = function(query, callback) {
		this.findDocumentsWithLimit(query, -1, callback);
	};

	this.findDocumentsWithLimit = function(query, limit, callback) {
		connectToDb(function(db) {
			var collection = db.collection(tablename);
			if (limit === undefined || limit === -1) {
				collection.find(query).toArray(function(errdb, docs) {
					if (callback) callback(errdb, docs);
				}, function(errdb1, result) {
					if (callback) callback(errdb1, result);
				});
			}
			else {
				collection.find(query).limit(limit).toArray(function(errdb, docs) {
					if (callback) callback(errdb, docs);
				}, function(errdb1, result) {
					if (callback) callback(errdb1, result);
				});
			}
		});
	};

	this.findDocumentById = function(uuid, callback) {
		connectToDb(function(db) {
			var collection = db.collection(tablename);
			collection.find({uuid: uuid}).toArray(function(errdb, docs) {
				if (docs.length > 0) {
					if (callback) callback(errdb, docs[0]);
				}
				else {
					if (callback) callback(errdb);
				}
			}, function(errdb1, result) {
				callback(errdb1, result);
			});
		});
	};

	this.findDocument = function(query, callback) {
		connectToDb(function(db) {
			var collection = db.collection(tablename);
			collection.find(query).toArray(function(errdb, docs) {
				if (docs.length > 0) {
					if (callback) callback(errdb, docs[0]);
				}
				else {
					if (callback) callback(errdb);
				}
			}, function(errdb1, result) {
				if (callback) callback(errdb1, result);
			});
		});
	};

	this.clearDatabase = function(callback) {
	    connectToDb(function(db) {
	        db.collectionNames(function(errdb, collections){
				if (!errdb) {
					async.each(collections, function(collection, icallback) {
						var name = collection.name.substring(dbname + ".".length);
						if (name.substring(0, 6) !== "system"){
							db.dropCollection(name, function(errdb1) {
								icallback(errdb1);
							});
						}
						else {
							if (callback) callback();
						}
					}, function(errdb1) {
						if (callback) callback(errdb1);
					});
				}
				else {
					callback(errdb);
					return;
				}
	        });
	    });
	};

	this.dropCollection = function(callback) {
	    connectToDb(function(db) {
	        db.dropCollection(tablename, function(errdb) {
	            if(errdb) {
					if (callback) callback(errdb, errdb.errmsg);
	            }
	            else {
	            	if (callback) callback();
	            }
	        });
	    });
	};

}

module.exports = Db;