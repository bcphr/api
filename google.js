var setAuth = function(callback) {
    var creds = require('./google.json');
    doc.useServiceAccountAuth(creds, callback);
}

var GoogleSpreadsheet = require('google-spreadsheet');

// spreadsheet key is the long id in the sheets URL
var doc;
var sheet;

var reservedKeys = ["_xml", "id", "save", "del", "app:edited", "_links", "_clrrx"];

var load = function(id, sheetId, callback) {
	doc =  new GoogleSpreadsheet(id);
	var result = [];
	setAuth(function() {
		doc.getInfo(function(err, info) {
			sheet = info.worksheets[sheetId];
			sheet.getRows({}, function( err, rows ){
				//iterate through first row to get headings
				var columns = [];
				if (rows[0]) {
					Object.keys(rows[0]).forEach(function(x) {
						if (reservedKeys.indexOf(x) === -1) {
							columns.push(x);
						}
					});
					result.push(columns);
					rows.forEach(function(row) {
						var rowArray = [];
						columns.forEach(function(x) {
							rowArray.push(row[x]);
						});
						result.push(rowArray);
					});
				}
				callback(result);
		    });
	    });
	});
}

module.exports = load;