// set variables for environment
var express = require("express"),
    app = express(),
    path = require("path"),
    favicon = require("serve-favicon"),
    bodyParser = require("body-parser"),
    fs = require("fs"),
    config = require("./config");
    cors = require("cors");
    compression = require("compression");

//utils
var sessions = require("./service/sessions");
var userSv = require("./service/users");
var codes = require("./codes");
var ev = require('express-validation');
//var socket = require("./socket");

var whitelist = [
    "http://localhost",
    "http://localhost:3001"
];

var corsOptions = {
    origin: function(origin, callback) {
        var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
        callback(null, originIsWhitelisted);
    },
    credentials: true,
};


app.use(bodyParser.raw({limit: "25mb"}));
app.use(bodyParser.json({limit: "25mb"}));
app.use(bodyParser.text({limit: "25mb"}));
app.use(bodyParser.urlencoded({limit: "25mb"}));
app.use(express.static("public"));
app.use(cors(corsOptions));
app.use(compression());

var checkExists = function(array, id) {
    return array.filter(function(x) {
        return x.userId === id && x.status === "enabled";
    }).length > 0
}

app.use(function(req, res, next) {
    if (req.method === "OPTIONS") {
        next();
    }
    else {
        var path = req.path || "";
        if (path.indexOf(codes.API_PREFIX + "/emails/") === 0) {
            //let it through, it's normal
            next();
        }
        else if (path.indexOf(codes.API_PREFIX + "/auth/") === 0) {
            //let it through, it's normal
            next();
        }
        else if (path.indexOf(codes.API_PREFIX + "/pub") === 0) {
            next();
        }
        else {
            sessions.getFullUser(req, function(err, user) {
                if (!err && user) {
                    var prefix = 2;
                    var pathTokens = path.split("/");
                    if (user.admin) {
                        next();
                    }
                    else if (path.indexOf(codes.API_PREFIX + "/private") === 0) {
                        next();
                    }
                    else if (path.indexOf(codes.API_PREFIX + "/users") === 0) {
                        next();
                    }
                    else if (path.indexOf(codes.API_PREFIX + "/admin") === 0) {
                        //must be a mark admin
                        res.status(403).send({code: "notadmin"});
                    }
                    else if (path.indexOf(codes.API_PREFIX + "/funders/admin/") === 0) {

                    }
                    else {
                        res.status(400).send({code: "invalidpath"})
                    }
                }
                else {
                    res.status(400).send({code: "invalidtoken"});
                }
            });
        }
    }
});


require('./api/public')(app);
require('./api/private')(app);
require('./api/users')(app);

app.use(function(err, req, res, next){
    if (err instanceof ev.ValidationError) return res.status(err.status).json(err);
});

// fs.exists('/ssl/myserver.key', function(exists){
//     if (exists) {
//         var https = require('https');
//         var options = {
//             key: fs.readFileSync('/ssl/myserver.key'),
//             cert: fs.readFileSync('/ssl/__worktango_io.crt'),
//             port: 3456
//         };
//         var server = https.createServer(options);
//         server.listen(3456);
//         io= require('socket.io').listen(server);
//         console.log('socket.io starting using options', options);
//     } else {
//         io= require('socket.io').listen(3456);
//         console.log('socket.io starting without SSL');
//     }
//     io.set('origins', '*:*');
//     io.set('transports', ['websocket']);
//     io.sockets.on('connection', socket.onConnect);
//     io.sockets.on('error', function (e) {
//         console.log('Socket.io exception', e);
//     });
// });

// Set server port
app.listen(config.server.port);
console.log("Server is running, with configuration:", config);
