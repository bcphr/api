var userDb = require("../db/users");


var userSv = require("../service/users");
var emailSv = require("../service/emails");
var sessionSv = require("../service/sessions");
var secSv = require("../service/security");
var uuid = require("node-uuid");
var db = require("../db/dbview");
var codes = require("../codes");
var ioSv = require("../service/io");
var config = require("../config");
var validate = require('express-validation');
var validation = require('./validation/users.js');

module.exports = function(app) {

    var createkeys = ["firstname","surname","email","password","image","preferences"];
    var updateablekeys = ["firstname","surname","email","password","image","preferences"];


    /**
     * @api {get} /users GetCurrentUser
     * @apiDescription Get Current User from session
     * @apiVersion 1.0.0
     * @apiName GetCurrentUser
     * @apiGroup Users
     *
     */
    app.get(codes.API_PREFIX + "/users/", function(req, res) {
        sessionSv.getUser(req, function(err, user, auser) {
            userSv.loadUser(req, user, auser, req.query.requester, sessionSv.getToken(req), function(resp) {
                db.processDb(err, user, res, resp);
            });
        });
    });

    /**
     * @api {get} /users/exists/:email CheckEmailExists
     * @apiDescription Check if Email exists on system
     * @apiVersion 1.0.0
     * @apiName CheckEmailExists
     * @apiGroup Users
     *
     * @apiParam {String} email The email of the user
     *
     */
    app.get(codes.API_PREFIX + "/users/exists/:email", function(req, res) {
        var query = userSv.searchQueryEmail(req.params.email);
        userDb.findDocument(query, function(err, user) {
            if (err) {
                db.processDb(err, user, res);
            }
            else {
                res.send(user ? {email: user.email } : {});
            }
        });
    });

    /**
     * @api {get} /users/:uuid GetUser
     * @apiDescription Get a User
     * @apiVersion 1.0.0
     * @apiName GetUser
     * @apiGroup Users
     *
     * @apiParam {String} uuid The id of the user
     * @apiParam (Query) {String=name,image} field Return firstname, surname, email if name, image if image
     *
     */
    app.get(codes.API_PREFIX + "/users/:uuid", validate(validation.userQ), function(req, res) {
        userDb.findDocumentById(req.params.uuid, function(err, resp) {
            if (!err && resp) {
                var result = {};
                user = userSv.getUser(resp);
                if (req.query.filter === "image") {
                    result.image = user.image;
                }
                else if (req.query.filter === "name") {
                    result.firstname = user.firstname;
                    result.surname = user.surname;
                    result.email = user.email;
                }
                res.send(result);
            }
            else {
                db.processDb(err, resp, res);
            }
        })
    });

    /**
     * @api {post} /users/logout LogoutUser
     * @apiDescription Log the User out
     * @apiVersion 1.0.0
     * @apiName LogoutUser
     * @apiGroup Users
     *
     *
     */
    app.post(codes.API_PREFIX + "/users/logout", function(req, res) {
        sessionSv.delete(req, function(err, resp) {
            res.send({});
        });
    });

    var loadSession = function(req, res, user, requester) {
        sessionSv.createSession(user, function(err, session) {
            if (!err && session) {
                userSv.loadUser(req, user, user, requester, session, function(resp) {
                    res.send(resp);
                })
            }
            else {
                res.status(503).send("SESSION DB ERROR");
            }
        })
    }

    /**
     * @api {post} /users/register RegisterUser
     * @apiDescription Register a user
     * @apiVersion 1.0.0
     * @apiName RegisterUser
     * @apiGroup Users
     *
     *
     */
    app.post(codes.API_PREFIX + "/auth/register/", validate(validation.register), function(req, res) {
        var requester = req.query.requester;
        var query = userSv.searchQueryEmail(req.body.email);
        userDb.findDocument(query, function(err, user) {
            if (user) {
                res.status(400).send({code: "userexists"});
            }
            else {
                var newuser = userSv.createUser(req.body.email, req.body.password);
                var params = {
                    name: userSv.getName(newuser),
                    code: secSv.encrypt(newuser.verifyCode)
                }
                emailSv.sendEmailTemplate([newuser.email], "Confirm Email", "verify", params, null);
                userDb.saveDocument(newuser, function(err, message) {
                    if (err && !message) {
                        res.status(503).send("USER DB ERROR");
                    }
                    else {
                        loadSession(req, res, newuser, requester);
                    }
                });
            }
        });
    });

    /**
     * @api {post} /users/verify/:code VerifyUser
     * @apiDescription Verify a user
     * @apiVersion 1.0.0
     * @apiName VerifyUser
     * @apiGroup Users
     *
     *
     */
    app.post(codes.API_PREFIX + "/auth/verify/:code", function(req, res) {
        var requester = req.query.requester;
        var query = {verifyCode: secSv.decrypt(req.params.code)};
        userDb.findDocument(query, function(err, user) {
            if (!user) {
                res.status(400).send({code: "usernotexists"});
            }
            else {
                user.verified = true;
                userDb.saveDocument(user, function(err, message) {
                    if (err && !message) {
                        res.status(503).send("USER DB ERROR");
                    }
                    else {
                        loadSession(req, res, user, requester);
                    }
                });
            }
        });
    });


    /**
     * @api {post} /auth/password ValidatePassword
     * @apiDescription Check if the password is the same as the password for the current user
     * @apiVersion 1.0.0
     * @apiName ValidatePassword
     * @apiGroup Users
     *
     * @apiParam (PasswordBody) {String} password The current password of the user
     *
     */
    app.post(codes.API_PREFIX + "/auth/password", validate(validation.password), function(req, res) {
        var password = secSv.encrypt(req.body.password);
        sessionSv.getFullUser(req, function(err, user) {
            if (password === user.password) {
                res.send({});
            }
            else {
                res.status(403).send({error: "PASSWORD MISMATCH"});
            }
        });
    });

    /**
     * @api {post} /auth/login LoginUser
     * @apiDescription Login a user
     * @apiVersion 1.0.0
     * @apiName LoginUser
     * @apiGroup Users
     *
     * @apiParam (LoginBody) {String} email The email of the user
     * @apiParam (LoginBody) {String} password the password of the user
     * @apiParam (Query) {String=funder,organisation} requester the source that wants to authenticate
     *
     */
    app.post(codes.API_PREFIX + "/auth/login", validate(validation.login), function(req, res) {
        var query = userSv.searchQueryEmail(req.body.email);
        query.password = secSv.encrypt(req.body.password);
        userDb.findDocument(query, function(err, user) {
            if (!err && user) {
                loadSession(req, res, user, req.query.requester);
            }
            else {
                res.status(404).send("USER NOT FOUND");
            }
        });
    });

    /**
     * @api {post} /users UpdateUser
     * @apiDescription Update the current User
     * @apiVersion 1.0.0
     * @apiName UpdateUser
     * @apiGroup Users
     *
     * @apiParam (UserBody) [String] firstname The firstname of the user
     * @apiParam (UserBody) [String] surname The surname of the user
     * @apiParam (UserBody) [String] email The email of the user
     * @apiParam (UserBody) [String] password The password of the user
     * @apiParam (UserBody) [Object] preferences A key value of preferences
     * @apiParam (UserBody) [Object:FileBody:image] image The profile image of the user
     *
     */
    app.post(codes.API_PREFIX + "/users", validate(validation.user), function(req, res) {
        sessionSv.getFullUser(req, function(err, user) {
            var input = req.body;
            input.uuid = user.uuid;
            if (input.password) {
                input.password = secSv.encrypt(req.body.password);
            }
            db.saveRecord(req, res, userDb, createkeys, updateablekeys, user, false, input);
        });
    });

    /**
     * @api {post} /auth/sendReset SendResetLink
     * @apiDescription Send a link to reset the password of a user
     * @apiVersion 1.0.0
     * @apiName SendResetLink
     * @apiGroup Users
     *
     * @apiParam (ResetBody) {String} email The email of the user
     * @apiParam (Query) {String=funder,organisation} requester the source that wants to authenticate
     *
     */
    app.post(codes.API_PREFIX + "/auth/sendReset", validate(validation.email), function(req, res) {
        userDb.findDocument(userSv.searchQueryEmail(req.body.email), function(err, user) {
            if (user) {
                user.verifyCode = uuid.v4();
                var params = {
                    name: userSv.getName(user),
                    code: secSv.encrypt(user.verifyCode)
                }
                emailSv.sendEmailTemplate([user.email], "Reset Password", "resetpw", params, null);
                db.saveRecord(req, res, userDb, createkeys, updateablekeys, user);
            }
            else {
                res.status(404).send("EMAIL NOT FOUND");
            }
        });
    });


    /**
     * @api {get} /auth/reset/:code ValidateResetLink
     * @apiDescription Validate the code sent in the reset password email
     * @apiVersion 1.0.0
     * @apiName ValidateResetLink
     * @apiGroup Users
     *
     * @apiParam {String} code The code sent in the reset password email
     *
     */
    app.get(codes.API_PREFIX + "/auth/reset/:code", function(req, res) {
        userDb.findDocument({verifyCode: secSv.decrypt(req.params.code)}, function(err, user) {
            if (user) {
                res.status(200).send("");
            }
            else {
                res.status(400).send({error:"INVALID CODE"});
            }
        });
    });

    /**
     * @api {post} /auth/reset/:code ResetPassword
     * @apiDescription Reset the password of the user
     * @apiVersion 1.0.0
     * @apiName ResetPassword
     * @apiGroup Users
     *
     * @apiParam {String} code The code sent in the reset password email
     * @apiParam (ResetBody) {String} password The updated password
     *
     */
    app.post(codes.API_PREFIX + "/auth/reset/:code", validate(validation.password), function(req, res) {
        userDb.findDocument({verifyCode: secSv.decrypt(req.params.code)}, function(err, user) {
            if (user) {
                user.password = secSv.encrypt(req.body.password);
                delete user.verifyCode;
                userDb.saveDocument(user, function(err, message) {
                    db.processDb(err, message, res, user);
                });
            }
            else {
                res.status(400).send({error:"INVALID CODE"});
            }
        });
    });
};
