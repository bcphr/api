var Joi = require('joi');

module.exports.file = {
  	options : {
    	allowUnknownBody: false
  	},
	body: {
      name: Joi.string().required(),
    	type: Joi.string().required(),
    	data: Joi.string().required()
  	}
};