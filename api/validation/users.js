var Joi = require('joi');

module.exports.register = {
  body: {
      email: Joi.string().required(),
      password: Joi.string().required()
  }
};


module.exports.user = {
  body: {
      email: Joi.string(),
      firstname: Joi.string(),
      surname: Joi.string(),
      password: Joi.string(),
      image: Joi.object().keys({
        uuid: Joi.string().required(),
        name: Joi.string().required(),
        format: Joi.string(),
        aws: Joi.string(),
        postfix: Joi.string()
      }),
      preferences: Joi.object()
  }
};

module.exports.email = {
	body: {
    	email: Joi.string().email().required()
  	}
};

module.exports.userQ = {
	query: {
    	filter: Joi.string().required()
  	}
};

module.exports.password = {
	body: {
    	password: Joi.string().required()
  }
};


module.exports.login = {
	body: {
		  email: Joi.string().email().required(),
    	password: Joi.string().required()
  	}
};
