var codes = require("../codes");
var db = require("../db/dbview");
var ioSv = require("../service/io");

module.exports = function(app) {

	/**
     * @api {get} /pub/view/:uuid ViewFile
     * @apiDescription View a File that has been uploaded
     * @apiVersion 1.0.0
     * @apiName ViewFile
     * @apiGroup Public
     *
     * @apiParam {String} uuid The Id of the file
     *
     */
	app.get(codes.API_PREFIX + "/pub/view/:uuid", function(req, res) {
        ioSv.view(req, res, req.params.uuid);
    });
};

