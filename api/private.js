var userDb = require("../db/users");

var userSv = require("../service/users");
var emailSv = require("../service/emails");
var sessionSv = require("../service/sessions");

var secSv = require("../service/security");
var uuid = require("node-uuid");

var db = require("../db/dbview");
var codes = require("../codes");
var ioSv = require("../service/io");

var validate = require('express-validation');
var validation = require('./validation/common.js');


module.exports = function(app) {

    /**
     * @api {post} /private/upload UploadFile
     * @apiDescription Upload a File
     * @apiVersion 1.0.0
     * @apiName UploadFile
     * @apiGroup Private
     *
     * @apiParam (UploadBody) {String} name The name of the file
     * @apiParam (UploadBody) {Base64} data The data of the file
     *
     */
    app.post(codes.API_PREFIX + "/private/upload", validate(validation.file), function(req, res) {
        var input = req.body;
        var filename = input.name;
        var postfixindex = filename.lastIndexOf(".");
        var postfix;
        if (postfixindex !== -1) {
            postfix = filename.substring(postfixindex);
        }
        var base64Data = input.data.replace(/^data:(.)*;base64,/, "");
        ioSv.upload(base64Data, 'base64', postfix, input, function(err, resp) {
            db.processDb(err, resp, res);
        });
    });

    /**
     * @api {post} /private/download/:uuid DownloadFile
     * @apiDescription Download a File uploaded
     * @apiVersion 1.0.0
     * @apiName DownloadFile
     * @apiGroup Private
     *
     * @apiParam {String} uuid The id of the file
     *
     */
    app.get(codes.API_PREFIX + "/private/download/:uuid", function(req, res) {
        ioSv.download(req, res, req.params.uuid);
    });
};
